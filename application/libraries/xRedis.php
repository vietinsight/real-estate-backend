<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class XRedis extends Redis {
	
	private $isConnected = FALSE;


	function __construct()
	{
		$CI =& get_instance();
		$config = $CI->load->config('redis', TRUE);

		try {
			if (parent::connect($config['host'], $config['port'])) {
				$this->isConnected = TRUE;
			}

			if (isset($config['database'])) {
				parent::select($config['database']);
			}
		}
		catch (Exception $e) {
			throw new Exception('Redis: Connect fail');
		}
	}


	public function delete($key)
	{
		if (strpos($key, '*') === FALSE) {
			parent::delete($key);
		}
		else {
			foreach (parent::keys($key) as $k) {
				parent::delete($k);
			}
		}
	}


	public function isConnected()
	{
		return $this->isConnected;
	}
}