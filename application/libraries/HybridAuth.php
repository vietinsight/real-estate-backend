<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . '/third_party/Hybrid/Auth.php');


class HybridAuth
{

    private $auth = NULL;

    function __construct()
    {
        $ci                 =& get_instance();
        $config             = include(APPPATH . '/config/hybrid_auth.php');
        $config['base_url'] = site_url('auth/index');
        $this->auth         = new Hybrid_Auth($config);
    }


    public function authenticate($provider = '')
    {
        $provider = ucfirst($provider);

        try {
            $auth = $this->auth->authenticate($provider);

            return $auth->getUserProfile();
        }
        catch (Exception $e) {
            var_dump($e->getMessage());
        }


        return FALSE;
    }
}