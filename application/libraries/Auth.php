<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Auth {

	private $ci;
	private $config;
	private $userId = null;
	private $payLoad = null;


	function __construct()
	{
		$this->ci =& get_instance();
		$this->ci->load->library('JWT');
		$this->config = $this->ci->load->config('auth', true);
	}


	public function generateKey($payload = [])
	{
		return $this->ci->jwt->encode($payload, $this->config['secret_key']);
	}


	public function generateToken($user, $timeout = null)
	{
		if (!isset($user->id)) {
			return false;
		}

		$payload = [
			'id' => (int) $user->id,
			'at' => $user->access_token,
			'iat' => time()
		];

		if (!is_null($timeout)) {
			$payload['timeout'] = $timeout;
		}

		return $this->ci->jwt->encode($payload, $this->config['secret_key']);
	}


	public function getHeader($header = null)
	{
		return $this->ci->input->get_request_header($header);
	}


	public function getToken()
	{
		return $this->getHeader('X-Auth-Token');
	}


	public function getPayload($item = null, $token = null)
	{
		if (is_null($token)) {
			$token = $this->getToken();
		} else {
			$this->payLoad = null;
		}

		if (empty($token)) {
			return null;
		}

		if (is_null($this->payLoad)) {
			try {
				$this->payLoad = $this->ci->jwt->decode($token, $this->config['secret_key']);
			} catch (\Exception $e) {
				$this->payLoad = null;
			}
		}

		if (is_null($item)) {
			return $this->payLoad;
		} else {
			return isset($this->payLoad->{$item}) ? $this->payLoad->{$item} : null;
		}
	}
	

	public function userId()
	{
		if (isset($this->ci->session)) {
			$this->userId = (int) $this->ci->session->userdata('userId');
		} else {
			if (is_null($this->userId)) {
				$this->userId = $this->getPayload('id');
			}
		}

		return $this->userId;
	}


	public function isLogined($type = null)
	{
		$userId = $this->userId();
		if (is_null($userId)) {
			return false;
		} else {
			$this->ci->load->model('Users');
			$user = $this->ci->Users->getDetailCache('id', $userId);

			return (!is_null($user) && (is_null($type) OR $type === $user->type));
		}
	}


	public function getTimeout()
	{
		return isset($this->payLoad->timeout) ? intval($this->payLoad->timeout) : $this->config['timeout'];
	}


	public function tokenValid()
	{
		$payload = $this->getPayload();

		if (is_null($payload)) {
			return null;
		} else {
			$timeout = $this->getTimeout();

			return ($payload->iat + $timeout) >= time();
		}
	}


	public function checkToken($printOutput = true)
	{
		$err = [];

		$tokenValid = $this->tokenValid();

		if (empty($this->getToken())) {
			$err = [
				'reason' => 'token_empty',
				'message' => 'Your token is empty. Authentication required'
			];
		} elseif (is_null($tokenValid)) {
			$err = [
				'reason' => 'token_invalid',
				'message' => 'Your token is invalid. Authentication required'
			];
		} elseif ($tokenValid === false) {
			$err = [
				'reason' => 'token_timeout',
				'message' => 'Your token is timeout. Authentication required'
			];
		}

		if (!empty($err)) {
	        header('Content-Type: application/json');
	        header('Access-Control-Allow-Origin: *');

	        echo json_encode(array_merge(['result' => false, 'status' => 401], $err));
	        die();
		}
	}
}