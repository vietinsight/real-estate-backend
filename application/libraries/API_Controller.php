<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';


class API_Controller extends REST_Controller {

    protected $selector;
    protected $model;
    public $data;
    public $where = [];


    function __construct()
    {
        parent::__construct();

        $apiKey = $this->input->get_request_header('X-API-Key');
        if (is_null($apiKey)) {
            $apiKey = $this->input->get('X-API-Key');
        }

        if ($this->input->ip_address() === '127.0.0.1') {
            $apiKey = 1;
        }

        if (is_null($apiKey)) {
            header('HTTP/1.0 403 Forbidden');
            echo json_encode([
                    'result' => false,
                    'status' => 403,
                    'reason' => 'no_api_key',
                    'message' => '403 Forbidden'
                ]);
            die();
        }

        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');

        $this->load->library('Auth');

        if (isset($this->model)) {
            $this->load->model($this->model, 'Model');
        }
    }


    public function responseDetail($item, $return = true) {

        if (is_null($item)) {
            $response = [
                'result' => false,
                'code' => 404,
                'reason' => 'not_found',
                'message' => 'The requested resource could not be found but may be available again in the future. Subsequent requests by the client are permissible.'
            ];
        } else {
            $response = [
                'result' => true,
                'code' => 200,
                'data' => $item
            ];
        }

        if ($return) {
            return $response;
        } else {
            $this->set_response($response);
        }
    }


    public function setData($data)
    {
        $this->data = $data;
    }


    public function setCondition($where)
    {
        $this->where = $where;
    }


    public function index_get($id = null)
    {
        if (is_null($id)) {
            $options = $this->getOptions();
            $response = $this->Model->getList($this->where, $options);
        } else {
            $field = is_numeric($id) ? 'id' : 'uuid';
            $item = $this->Model->getDetailByField($field, $id, $this->selector);
            $response = $this->responseDetail($item, true);
        }

        $this->set_response($response);
    }


    public function index_post()
    {
        if (empty($this->data)) {
            $data = $this->input->post_stream();
        } else {
            $data = $this->data;
        }
        $res = $this->Model->create($data);

        if (is_int($res) && $res > 0) {
            $response = [
                'result' => true,
                'status' => 201,
                'data' => $this->Model->getDetail($res, $this->selector)
            ];
        } else {
            $response = [
                'result' => false,
                'status' => 400,
                'errors' => $res,
                'message' => 'Bad Request: validation errors, missing or exist data'
            ];
        }

        $this->set_response($response);
    }


    public function index_put($id = null)
    {
        $data = $this->input->post_stream();
        $field = is_numeric($id) ? 'id' : 'uuid';
        $result = $this->Model->edit($data, [$field => $id]);

        $response = [
            'result' => $result === true
        ];

        if ($result === true) {
            $result = $this->Model->getDetailByField($field, $id, $this->selector);
            if ($result) {
                $response['code'] = 200;
                $response['data'] = $result;
            } else {
                $response['result'] = false;
                $response['status'] = 400;
            }
        } else {
            $response['status'] = 400;
            $response['errors'] = $result;
            $response['message'] = 'Bad Request: validation errors, missing or exist data';
        }

        $this->set_response($response);
    }


    public function index_patch($id = null)
    {
        return $this->index_put($id);
    }


    public function index_delete($id = null)
    {
        if (is_null($id)) {
            $response = [
                'result' => false,
                'code' => 400,
                'reason' => 'not_found'
            ];
        } else {
            $field = is_numeric($id) ? 'id' : 'uuid';
            $res = $this->Model->remove([$field => $id]);
            $response = [
                'result' => true,
                'code' => 200
            ];
        }

        $this->set_response($response);
    }


    protected function getOptions()
    {
        $options = [
            'selector' => $this->selector
        ];

        $gets = $this->input->get();
        $pagings = [
            'limit',
            'offset',
            'since',
            'page'
        ];
        foreach ($pagings as $item) {
            if (!empty($gets[$item])) {
                $options[$item] = $gets[$item];
            }
        }

        if (!empty($gets['filters'])) {
            $filter = json_decode($gets['filters'], true);
            if ($filter) {
                $options['filter'] = $filter;
            }
        }

        if (!empty($gets['sorts'])) {
            $sorts = json_decode($gets['sorts'], true);
            if ($sorts) {
                $options['order_by'] = $sorts;
            }
        }

        return $options;
    }

}
