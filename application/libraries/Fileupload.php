<?php

include APPPATH . '/third_party/UploadHandler.php';
include APPPATH . '/third_party/ImageResize.php';

ini_set('gd.jpeg_ignore_warning', true);

class Fileupload {

	private $ci;

	private $uploadDir, $uploadUrl, $storageDir, $uniqueId;


	function __construct()
	{
		$this->ci =& get_instance();
		$config = $this->ci->load->config('fileupload', true);

		$this->uniqueId = $this->getUniqueId();
		$this->uploadDir = $config['uploadDir'] . '/' . $this->uniqueId . '/';
		$this->uploadUrl = site_url($this->uploadDir) . '/';
		$this->storageDir = $config['storageDir'];
		$this->coverFileName = $config['coverFileName'];
	}


	public function getUniqueId() {
		$this->ci->load->library('Auth');
		$token = $this->ci->auth->getToken();
		if (is_null($token)) {
			$this->uniqueId = $this->ci->input->post_stream('unique_id');
			if (empty($this->uniqueId)) {
				$this->uniqueId = false;
			}
			return $this->uniqueId;
		} else {
			return 'u' . $this->ci->auth->userId() . '_' . md5($this->ci->auth->getToken());
		}
	}


	public function getListFiles()
	{
		$data = [];
		$files = glob($this->uploadDir . '*.{jpg,jpeg,gif,png,JPG,JPEG,GIF,PNG}', GLOB_BRACE);
		foreach ($files as $key => $file) {
			$fileName = basename($file);
			$fileUrl = site_url($file);
			$data[] = [
				'name' => $fileName,
				'size' => filesize($file),
				'type' => mime_content_type($file),
				'url' => $fileUrl,
				'thumbnailUrl' => str_replace($fileName, 'thumbnail/' . $fileName, $fileUrl),
				'deleteUrl' => site_url('api/upload') . '?file=' . basename($file),
				'deleteType' => 'DELETE'
			];
		}

		return $data;
	}


	public function clearDraft()
	{
		return rrmdir($this->uploadDir);
	}


	public function handler()
	{
		if ($this->uniqueId) {
			$options = [
				'script_url' => site_url('api/upload'),
				'upload_dir' => $this->uploadDir,
				'upload_url' => $this->uploadUrl,
				'accept_file_types' => '/(\.|\/)(gif|jpe?g|png)$/i',
				'max_file_size' => 10000000,
				'image_versions' => [
	                'medium' => [
	                    'max_width' => 480,
	                    'max_height' => 360
	                ],
					'thumbnail' => [
						'max_width' => 100,
						'max_height' => 100
					]
				]
			];

			if (!isset($_SERVER['HTTP_ACCEPT'])) {
				$_SERVER['HTTP_ACCEPT'] = 'application/json';
			} elseif (isset($_SERVER['HTTP_ACCEPT']) && strpos($_SERVER['HTTP_ACCEPT'], 'application/json') === false) {
				$_SERVER['HTTP_ACCEPT'] = 'application/json,' . $_SERVER['HTTP_ACCEPT'];
			}

			return new UploadHandler($options);
		} else {
			return false;
		}
	}


	public function saveImage($destPath)
	{
		$files = getFiles($this->uploadDir);
		if (!empty($files)) {
			$dir = $this->storageDir . '/' . $destPath;

			if (!is_dir($dir)) {
				$old = umask(0);
				@mkdir($dir, 0777, true);
				umask($old);
			}
			$exts = array('jpg', 'jpeg', 'gif', 'png');
			foreach ($files as $file) {
				$destFile = $dir .  '/' . substr($file, strlen($this->uploadDir));
				$info = new SplFileInfo($file);
				$ext = strtolower($info->getExtension());

				if (!is_dir(dirname($destFile))) {
					$old = umask(0);
					@mkdir(dirname($destFile), 0777, true);
					umask($old);
				}

				copy($file, $destFile);
			}

			if (in_array($ext, $exts)) {
				$image = new Eventviva\ImageResize($file);
				$image->resizeToWidth(200);
				$image->save($dir . '/' . $this->coverFileName, IMAGETYPE_PNG);
			}
		}

		removeDirectory($this->uploadDir);

		return empty($files) !== false;
	}

}