<?php

include APPPATH . '/third_party/PHPMailer/PHPMailerAutoload.php';


class Mailer extends PHPMailer {


	function __construct()
	{
		parent::__construct();


		//$mail->SMTPDebug = 3;                               // Enable verbose debug output

		$this->isSMTP();                                      // Set mailer to use SMTP
		// $this->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
		// $this->SMTPAuth = true;                               // Enable SMTP authentication
		// $this->Username = 'quochung108@gmail.com';                 // SMTP username
		// $this->Password = 'txoirjedyimymjzf';                           // SMTP password
		// $this->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		// $this->Port = 587;                                    // TCP port to connect to

		$this->Host = 'mail.privateemail.com';  // Specify main and backup SMTP servers
		$this->SMTPAuth = true;                               // Enable SMTP authentication
		$this->Username = 'contact@smartproperty.online';                 // SMTP username
		$this->Password = 'Smartleads1234';                           // SMTP password
		$this->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
		$this->Port = 465;                                    // TCP port to connect to

		$this->setFrom($this->Username, 'SmartProperty');
		// $this->addAddress('hungntq@adtop.vn', 'QH');     // Add a recipient
		$this->addReplyTo($this->Username, 'SmartProperty');
		// $this->addCC('cc@example.com');
		// $this->addBCC('bcc@example.com');
		$this->isHTML(true);                                  // Set email format to HTML
		$this->CharSet = 'UTF-8';

		// $this->Subject = 'Here is the subject';
		// $this->Body    = 'This is the HTML message body <b>in bold!</b>';
		// $this->AltBody = 'This is the body in plain text for non-HTML mail clients';
		// $this->SMTPDebug = 4;
	}


	public function setTitle($title)
	{
		$this->Subject = $title;

		return $this;
	}


	public function setBody($content)
	{
		$this->Body = $content;
		// $this->AltBody = strip_tags($content);

		return $this;
	}


	public function add($emails, $name = '')
	{
		if (is_string($emails)) {
			$emails = [$emails => $name];
		}

		foreach ($emails as $email => $name) {
			if (is_numeric($email)) {
				$this->addAddress($name);
			} else {
				$this->addAddress($email, $name);
			}
		}

		return $this;
	}


	public function attachment($paths, $name = '')
	{
		if (is_string($paths)) {
			$paths = [$paths => $name];
		}

		foreach ($paths as $key => $value) {
			if (is_numeric($key)) {
				$this->addAttachment($value, '');
			} else {
				$this->addAttachment($key, $value);
			}
		}

		return $this;
	}


	public function send()
	{
		return parent::send() ? true : $this->ErrorInfo;
	}
}