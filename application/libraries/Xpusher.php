<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Xpusher extends Pusher {

	private $config;


	function __construct()
	{
		$CI =& get_instance();
		$this->config = $CI->load->config('pusher', TRUE);

		parent::__construct($this->config['app_key'], $this->config['app_secret'], $this->config['app_id'], $this->config['options']);
	}


	public function getConfig() {
		return $this->config;
	}
}