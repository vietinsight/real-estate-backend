<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Esearch {

	private $ci;
	private $config;
	private $indexDefault;
	private $client;
	private $esParams = [];


	function __construct()
	{
		$this->ci =& get_instance();
		$this->client = Elasticsearch\ClientBuilder::create()->build();
		$config = $this->ci->config->load('elastic_search', true);
		$this->indexDefault = $config['index'];
		$this->selectIndex($this->indexDefault);
	}


	public function selectIndex($index)
	{
		$this->esParams['index'] = $index;
	}


	public function selectType($type)
	{
		$this->esParams['type'] = $type;
	}


	public function get($id, $type = null)
	{
		if (!is_null($type)) {
			$this->selectType($type);
		}

		return $this->client->get($this->esParams);
	}


	public function search($body = null)
	{
		$params = $this->esParams;
		if (!empty($body)) {
			$params = array_merge($params, ['body' => $body]);
		}
		return $this->client->search($params);
	}


	public function delete($id, $type = null)
	{
		if (!is_null($type)) {
			$this->selectType($type);
		}

		return $this->client->delete($this->esParams);
	}


	public function deleteIndex($index = null)
	{
		if (!is_null($index)) {
			$params = [
				'index' => $index
			];
		} else {
			$params = [
				'index' => $this->indexDefault
			];
		}

		return $this->client->indices()->delete($params);
	}


	public function index($data, $id = null)
	{
		if (is_null($id)) {
			$id = isset($data['_id']) ? $data['_id'] : $data['id'];
		}

		$this->esParams['body'] = $data;

		return $this->client->index($this->esParams);
	}


	public function indexBulk($rows, $size = 1000)
	{
		
	}
}