<?php
    $fields = [];
    foreach ($rows[0] as $key => $value) {
        $fields[] = $key;
    }
?>
<h3>Table <?php echo $table ?></h3>
<table border="1">
    <thead>
        <tr>
            <?php foreach ($fields as $field): ?>
            <th><?php echo $field ?></th>
            <?php endforeach; ?>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($rows as $row): ?>
    <tr>
        <?php foreach ($row as $k => $v): ?>
            <td><?php echo $v ?></td>
        <?php endforeach; ?>
    </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<br><br>