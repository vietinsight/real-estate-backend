<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!--[if !mso]><!-->
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!--<![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <style>
        body {
            margin: 0 !important;
            padding: 0;
            background-color: #ffffff;
        }
        table {
            border-spacing: 0;
            font-family: sans-serif;
            color: #333333;
        }
        td {
            padding: 0;
        }
        img {
            border: 0;
        }
        div[style*="margin:16px 0"] {
            margin: 0 !important;
        }
        .wrapper {
            width: 100%;
            table-layout: fixed;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }
        .webkit {
            max-width: 800px;
            margin: 0 auto;
        }
        .outer {
            margin: 1em auto;
            width: 100%;
            max-width: 800px;
        }
        .mgb-10 {
            margin-bottom: 10px;
        }
        .mgb-20 {
            margin-bottom: 20px;
        }
        .mgb-30 {
            margin-bottom: 30px;
        }
        .logo {
            text-align: center;
        }
        .inner {
            padding: 10px;
        }
        .h4 {
            text-align: center;
            font-weight: normal;
            font-size: 16px;
            margin-bottom: 15px;
        }
        .h6 {
            font-weight: normal; 
            font-size: 15px;
        }
        p {
            margin: 0;
        }
        a {
            color: #ee6a56;
            text-decoration: underline;
        }
        .strong {
            font-weight: 700;
        }
        .highlight {
            color: #42B149;
        }
        .ul-list {
            margin-bottom: 30px;
            padding-left: 20px;
            list-style-type: disc;
        }
        .ul-list li {
            margin-bottom: 3px;
        }
        .ul-list-2 {
            text-align: center; 
            font-size: 0px;
            padding-left: 0;
        }
        .ul-list-2 li {
            display: inline-block; 
            margin: 0px 15px;
        }
        .right-sidebar {
            text-align: center;
            font-size: 0;
        }
        .right-sidebar .column {
            width: 100%;
            display: inline-block;
            vertical-align: middle;
        }
        .right-sidebar .left {
            max-width: 550px;
        }
        .right-sidebar .contents {
            font-size: 17px;
            text-align: left;
        }
        .right-sidebar a {
            color: #42B149;
        }
        .right-sidebar .right {
            max-width: 250px;
        }
        .right-sidebar .img {
            width: 100%;
            height: auto;
        }
        .sp-footer {
            background-color: #99c065;
            color: #fff;
            height: 32px;
            margin-top: 1.5em;
            padding-top: 0.8em;
            text-align: center;
        }
        .sp-footer a {
            color: #FFF;
        }
    </style>
    <!--[if (gte mso 9)|(IE)]>
    <style type="text/css">
        table {border-collapse: collapse !important;}
    </style>
    <![endif]-->

    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,700&subset=latin-ext" rel="stylesheet" />
</head>
<body style="margin-top:0 !important;margin-bottom:0 !important;margin-right:0 !important;margin-left:0 !important;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;background-color:#ffffff;" >
    <center class="wrapper" style="width:100%;table-layout:fixed;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;" >
        <div class="webkit" style="max-width:800px;margin-top:0;margin-bottom:0;margin-right:auto;margin-left:auto;" >
            <!--[if (gte mso 9)|(IE)]>
            <table width="800" align="center" style="border-spacing:0;font-family:sans-serif;color:#333333;" >
            <tr>
            <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" >
            <![endif]-->
            <table class="outer" align="center" style="border-spacing:0;font-family:sans-serif;color:#333333;margin-top:1em;margin-bottom:1em;margin-right:auto;margin-left:auto;width:100%;max-width:800px;" >
                <tr>
                    <td class="logo" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;text-align:center;" >
                        <a target="_blank" href="<?php echo $web_url ?>" style="color:#ee6a56;text-decoration:underline;" ><img src="cid:logo" alt="Smart Property" style="border-width:0;" /></a>
                    </td>
                </tr>
                <tr>
                    <td class="right-sidebar" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;text-align:center;font-size:0;" >
                        <!--[if (gte mso 9)|(IE)]>
                        <table width="100%" style="border-spacing:0;font-family:sans-serif;color:#333333;" >
                        <tr>
                        <td width="550" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" >
                        <![endif]-->
                        <div class="column left" style="width:100%;display:inline-block;vertical-align:middle;max-width:550px;" >
                            <table width="100%" style="border-spacing:0;font-family:sans-serif;color:#333333;" >
                                <tr>
                                    <td class="inner contents" style="padding-top:10px;padding-bottom:10px;padding-right:10px;padding-left:10px;font-size:17px;text-align:left;" >
                                        <p class="mgb-30" style="margin-top:0;margin-right:0;margin-left:0;margin-bottom:30px;" >Welcome <strong><?php echo $name ?></strong>!</p>

                                        <?php if (empty($social)): ?>
                                        <p class="mgb-30" style="margin-top:0;margin-right:0;margin-left:0;margin-bottom:30px;" >It’s great to have you with us.  We want to be sure that this email belongs to you. </p>

                                        <p>
                                            <a target="_blank" href="<?php echo $link ?>" style="text-decoration:underline;color:#42B149;" >Click to confirm your email address</a>
                                        </p>
                                        <br/>

                                        <p>
                                            Username: <span class="strong highlight" style="font-weight:700;color:#42B149;" ><?php echo $email ?></span>
                                        </p>
                                        <p class="mgb-30" style="margin-top:0;margin-right:0;margin-left:0;margin-bottom:30px;" >Verifying your email allows you to start listing and gets instant notifications when homes your customers like hit the market.</p>
                                        <?php endif; ?>

                                        <p class="mgb-30" style="margin-top:0;margin-right:0;margin-left:0;margin-bottom:30px;" >Email us at <i>contact@smartproperty.online</i> should you need help.</p>

                                        <?php if ($type === 'Agent'): ?>

                                        <p class="mgb-30" style="margin-top:0;margin-right:0;margin-left:0;margin-bottom:30px;" ><strong>Smart Property</strong> improves service delivery at each touch point.&nbsp;The result?&nbsp;Buyers’ searching process is shortened.  More importantly, your sales cycle is shortened as a result!</p>
                                        <ul class="ul-list" style="margin-bottom:30px;padding-left:20px;list-style-type:disc;" >
                                            <li style="margin-bottom:3px;" ><span class="strong highlight" style="font-weight:700;color:#42B149;" >Snap and List</span> your properties on the spot</li>
                                            <li style="margin-bottom:3px;" ><span class="strong highlight" style="font-weight:700;color:#42B149;" >In-App Chat</span> real time for faster customer conversion</li>
                                            <li style="margin-bottom:3px;" ><span class="strong highlight" style="font-weight:700;color:#42B149;" >Smart Appointment</span> enables your customers to book mutually available viewing slots</li>
                                            <li style="margin-bottom:3px;" ><span class="strong highlight" style="font-weight:700;color:#42B149;" >Snap Chart</span> shows short listed properties’ features and costs in 1 easy-to-read table</li>
                                            <li style="margin-bottom:3px;" ><span class="strong highlight" style="font-weight:700;color:#42B149;" >Agent Dashboard</span> summarizes your time-sensitive priorities for the day and week so you can be on top of your games!</li>
                                        </ul>
                                        <p class="mgb-20" style="margin-top:0;margin-right:0;margin-left:0;margin-bottom:20px;" >Happy Selling! <br>The Smart Property Team</p>

                                        <?php elseif ($type === 'Seller'): ?>

                                        <p class="mgb-30" style="margin-top:0;margin-right:0;margin-left:0;margin-bottom:30px;" ><strong>Smart Property</strong> takes the stress out of selling your own houses.  We enable you to shorten the sales cycle with information at your finger-tips.</p>
                                        <ul class="ul-list" style="margin-bottom:30px;padding-left:20px;list-style-type:disc;" >
                                            <li style="margin-bottom:3px;" ><span class="strong highlight" style="font-weight:700;color:#42B149;" >Snap and List</span> your properties on the spot</li>
                                            <li style="margin-bottom:3px;" ><span class="strong highlight" style="font-weight:700;color:#42B149;" >In-App Chat</span> real time for faster customer conversion</li>
                                            <li style="margin-bottom:3px;" ><span class="strong highlight" style="font-weight:700;color:#42B149;" >Smart Appointment</span> enables your customers to book mutually available viewing slots</li>
                                            <li style="margin-bottom:3px;" ><span class="strong highlight" style="font-weight:700;color:#42B149;" >Comparative Market Analysis:</span> Like it or not, you are competing with other sellers in the area. Use CMA to price right!</li>
                                            <li style="margin-bottom:3px;" ><span class="strong highlight" style="font-weight:700;color:#42B149;" >Customer Care:</span> 1-stop shop for you to manage customer inquiries, appointments, and status on the go.</li>
                                        </ul>
                                        <p class="mgb-20" style="margin-top:0;margin-right:0;margin-left:0;margin-bottom:20px;" >Happy Selling! <br>The Smart Property Team</p>

                                        <?php elseif ($type === 'Buyer'): ?>

                                        <p class="mgb-30" style="margin-top:0;margin-right:0;margin-left:0;margin-bottom:30px;" >Finding your dream home is hard but it doesn’t have to be. We designed this platform with you in mind.  By improving service delivery at each touch point, we shorten your searching process.  Our Pay per Listing model removes outdated listings in the Subscription Model.  With rating capabilities on Listings and Agents, transparency can be sowed by the Community. Help your peers by putting in your ratings too. Platform rating will help us improve continuously.</p>
                                        <ul class="ul-list" style="margin-bottom:30px;padding-left:20px;list-style-type:disc;" >
                                            <li style="margin-bottom:3px;" ><span class="strong highlight" style="font-weight:700;color:#42B149;" >Powerful Search:</span> Enter where you want to live then get quality listings in return to short list and compare with ease.</li>
                                            <li style="margin-bottom:3px;" ><span class="strong highlight" style="font-weight:700;color:#42B149;" >Ratings:</span> Leverage ratings to prioritize your searching process</li>
                                            <li style="margin-bottom:3px;" ><span class="strong highlight" style="font-weight:700;color:#42B149;" >Comparative Market Analysis:</span> Broaden your search to see comparable property features and costs nearby.  Be in the know and maximize your capital investment!</li>
                                            <li style="margin-bottom:3px;" ><span class="strong highlight" style="font-weight:700;color:#42B149;" >Snap Chart:</span> Saves you time by consolidating your short listed properties’ features and costs in 1 easy-to-read table.</li>
                                        </ul>
                                        <p class="mgb-20" style="margin-top:0;margin-right:0;margin-left:0;margin-bottom:20px;" >Have a great day! <br>The Smart Property Team</p>

                                        <?php endif; ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <!--[if (gte mso 9)|(IE)]>
                        </td><td width="250" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" >
                        <![endif]-->
                        <div class="column right" style="width:100%;display:inline-block;vertical-align:middle;max-width:250px;" >
                            <table width="100%" style="border-spacing:0;font-family:sans-serif;color:#333333;" >
                                <tr>
                                    <td class="inner" style="padding-top:10px;padding-bottom:10px;padding-right:10px;padding-left:10px;" >
                                        <img class="img mgb-10" src="cid:banner" alt="img" style="border-width:0;margin-bottom:10px;width:100%;height:auto;" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <!--[if (gte mso 9)|(IE)]>
                        </td>
                        </tr>
                        </table>
                        <![endif]-->
                    </td>
                </tr>
                <tr>
                    <td class="one-column" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" >
                        <table width="100%" style="border-spacing:0;font-family:sans-serif;color:#333333;" >
                            <tr>
                                <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" >
                                    <p class="h4" style="margin-top:0;margin-right:0;margin-left:0;text-align:center;font-weight:normal;font-size:16px;margin-bottom:15px;" >SmartProperty is on iOS and Android. <br>Snap property with ease!</p>
                                    <ul class="ul-list-2" style="text-align:center;font-size:0px;padding-left:0;" >
                                        <li style="display:inline-block;margin-top:0px;margin-bottom:0px;margin-right:15px;margin-left:15px;" >
                                            <a target="_blank" href="<?php echo $web_url ?>" style="color:#ee6a56;text-decoration:underline;" ><img alt="img" src="cid:app_store" style="border-width:0;" ></a>
                                        </li>
                                        <li style="display:inline-block;margin-top:0px;margin-bottom:0px;margin-right:15px;margin-left:15px;" >
                                            <a target="_blank" href="<?php echo $web_url ?>" style="color:#ee6a56;text-decoration:underline;" ><img alt="img" src="cid:google_play" style="border-width:0;" ></a>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="one-column" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" >
                        <table width="100%" style="border-spacing:0;font-family:sans-serif;color:#333333;" >
                            <tr>
                                <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" >
                                    <div class="sp-footer" style="background-color:#99c065;color:#fff;height:32px;margin-top:1.5em;padding-top:0.8em;text-align:center;" >
                                        <p class="h6" style="margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;font-weight:normal;font-size:15px;" >Copyright &copy; 2016 - Smart Leads Pte. Ltd. - <a target="_blank" href="<?php echo $web_term_privacy ?>" style="text-decoration:underline;color:#FFF;" >Privacy Policy</a> &nbsp;&nbsp; <a target="_blank" href="<?php echo $web_unsubscribe ?>" style="text-decoration:underline;color:#FFF;" >Unsubscribe to Email Service</a></p class="h6">
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </div>
    </center>
</body>
</html>