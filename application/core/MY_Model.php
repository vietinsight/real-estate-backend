<?php defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH . '/third_party/MY_XModel.php';


class MY_Model extends MY_XModel
{

    protected $columns = [];
    protected $data = [];
    protected $limit = 20;


    public function getColumns()
    {
        if (empty($this->columns)) {
            $this->columns = $this->db->list_fields($this->table);
        }

        return $this->columns;
    }


    public function columnIsExist($columnName)
    {
        $columns = $this->getColumns();

        return in_array($columnName, $columns);
    }


    public function getMappingFields($field = NULL)
    {
        if (isset($this->fieldMapping)) {
            return is_null($field) ? $this->fieldMapping : $this->fieldMapping[$field];
        } else {
            return [];
        }
    }


    public function assignData($data = [], $extendData = [])
    {
        $this->getColumns();
        $data = array_merge($data, $extendData);
        if (empty($data)) {
            $data = $this->input->post_stream();
        }
        $assignData = [];
        foreach ($this->columns as $field) {
            if (isset($data[$field])) {
                $assignData[$field] = $data[$field];
            }
        }

        $this->data = $assignData;

        return $this->data;
    }


    public function formValidation($data = [], $group = null)
    {
        if (!isset($this->fieldValidation)) {
            return true;
        }

        if (empty($data)) {
            $data = $this->data;
        }
        if (empty($data)) {
            return [
                'code' => 4001,
                'reason' => 'data_is_empty'
            ];
        }

        $this->load->library('form_validation');
        $this->form_validation->set_data($data);

        if (is_null($group)) {
            $fieldRules = $this->fieldValidation;
        } elseif ($group === 'update') {
            $fieldRules = [];
            foreach ($data as $key => $value) {
                if (isset($this->fieldValidation[$key])) {
                    $fieldRules[$key] = $this->fieldValidation[$key];
                }
            }
        } elseif (!empty($this->groupValidation[$group])) {
            $fieldRules = [];
            foreach ($this->groupValidation[$group] as $field) {
                $fieldRules[$field] = $this->fieldValidation[$field];
            }
        }

        if (!empty($fieldRules)) {
            foreach ($fieldRules as $field => $rules) {
                $this->form_validation->set_rules($field, '', $rules);
            }

            if ($this->form_validation->run() === false) {
                $errors = [];
                foreach ($this->form_validation->error_array() as $key => $value) {
                    $errors[] = [
                        'code' => 400,
                        'field' => $key,
                        'message' => $value
                    ];
                }
                return $errors;
            } else {
                return $this->checkIndexes($data);
            }
        }

        return true;
    }


    public function checkIndexes($data = [])
    {
        if (empty($data)) {
            $data = $this->data;
        }

        $i = 0;
        if (!empty($this->indexes)) {
            $indexes = [];

            foreach ($this->indexes as $idx => $index) {
                $group = explode('|', $index);
                if (count($group) === 1) {
                    if (!empty($data[$index])) {
                        $whereMethod = ($i === 0) ? 'where' : 'or_where';
                        $this->db->{$whereMethod}($index, $data[$index]);
                        $indexes[] = $index;
                        $i++;
                    }
                } else {
                    $f = [];
                    foreach ($group as $_index) {
                        if (isset($data[$_index])) {
                            $f[$_index] = $data[$_index];
                        }
                    }

                    if (count($f) === count($group)) {
                        $groupMethod = ($i === 0) ? 'group_start' : 'or_group_start';
                        $this->db->{$groupMethod}();
                        $this->db->where($f);
                        $indexes[] = $group;
                        $this->db->group_end();
                        $i++;
                    }
                }
            }

            if ($i > 0) {
                $result = $this->db->get($this->table)->result();
                if (empty($result)) {
                    return true;
                } else {
                    $errors = [];

                    foreach ($result as $row) {
                        foreach ($indexes as $index) {
                            if (is_array($index)) {
                                $j = 0;
                                foreach ($index as $idx) {
                                    if ($data[$idx] === $row->{$idx}) {
                                        $j++;
                                    }
                                }

                                if ($j > 0) {
                                    $errors[] = [
                                        'code' => 409,
                                        'field' => $index,
                                        'message' => 'The group data [' . implode(' - ', $index) . '] is exist'
                                    ];
                                }
                            } else {
                                if ($data[$index] === $row->{$index}) {
                                    $errors[] = [
                                        'code' => 409,
                                        'field' => $index,
                                        'message' => 'The field ' . $index . ' is exist'
                                    ];
                                }
                            }
                        }
                    }

                    return $errors;
                }
            }
        }

        return true;
    }


    public function convertInput($data = [])
    {
        if (!isset($this->fieldMapping)) {
            return $data;
        }

        foreach ($data as $key => $value) {
            if (isset($this->fieldMapping[$key])) {
                $outputType = $this->fieldMapping[$key];
                
                if ($outputType === 'Object' OR $outputType === 'Array') {
                    $data[$key] = (!is_scalar($value) && !empty($value)) ? json_encode($value) : null;
                } else {
                    if (isset($this->fieldMapping[$key][$value])) {
                        $data[$key] = $value;
                    } else {
                        $mapping = array_flip($this->fieldMapping[$key]);
                        if (isset($mapping[$value])) {
                            $data[$key] = $mapping[$value];
                        } else {
                            unset($data[$key]);
                        }
                    }
                }
            }
        }

        return $data;
    }


    public function convertOutput($data = [])
    {
        if (!isset($this->fieldMapping)) {
            return $data;
        }

        foreach ($data as $key => $value) {
            if (isset($this->fieldMapping[$key])) {
                $outputType = $this->fieldMapping[$key];

                if ($outputType === 'Object' OR $outputType === 'Array') {
                    $data->$key = (is_string($value) && !empty($value)) ? json_decode($value) : ($outputType === 'Array' ? [] : null);
                } else {
                    if (isset($outputType[$value])) {
                        $data->$key = $outputType[$value];
                    }
                }
            }
        }

        return $data;
    }


    protected function getIdv()
    {
        if (!isset($this->idv_length)) {
            $this->idv_length = 6;
        }
        $idv = '';
        while ($idv === '') {
            $randomString = strtolower(randomString($this->idv_length));
            $chk = $this->db->where('idv', $randomString)->count_all_results($this->table);
            if ($chk === 0) {
                $idv = $randomString;
            }
        }
        
        return $idv;
    }


    public function create($data = [], $extendData = [])
    {
        $formValidation = $this->formValidation($data);
        $data = $this->assignData($data, $extendData);
        if ($formValidation === true) {

            if (in_array('idv', $this->columns)) {
                $data['idv'] = $this->getIdv();
            }

            if (in_array('uuid', $this->columns)) {
                $this->db->set('uuid', 'UUID()', false);
            }

            if (in_array('user_id', $this->columns) && !is_null($this->auth->userId())) {
                $data['user_id'] = $this->auth->userId();
            }
            
            if (in_array('time_created', $this->columns)) {
                $data['time_created'] = date('Y-m-d H:i:s');
            }

            $data = $this->convertInput($data);
            $id = $this->insert($data);

            if (method_exists($this, 'afterUpdate')) {
                $this->afterUpdate(['id' => $id]);
            }

            $this->uploadHandler($id);

            return $id;
            
        } else {

            return $formValidation;
        }
    }


    public function edit($data = [], $extendData = [])
    {
        $conditions = [];
        $id = $this->input->post_stream('id');

        if (is_numeric($id)) {
            $conditions['id'] = $id;
        } elseif (!empty($extendData['id'])) {
            $conditions['id'] = (int) $extendData['id'];
            $id = $conditions['id'];
        } elseif (!empty($extendData['uuid'])) {
            $obj = $this->getDetailCache('uuid', $extendData['uuid']);
            if (!is_null($obj)) {
                $id = (int) $obj->id;
                $conditions['id'] = $id;
            }
        }

        if (empty($conditions)) {
            return false;
        }
        
        $saveFile = $this->uploadHandler($id);
        $formValidation = $this->formValidation($data, 'update');
        if (!empty($data) && $formValidation !== true) {
            return $formValidation;
        }
        $data = $this->assignData($data, $extendData);

        $data = array_merge($data, $extendData);
        unset($data['id'], $data['uuid']);

        if (!empty($data) && !empty($conditions)) {

            if (in_array('time_updated', $this->columns)) {
                $data['time_updated'] = date('Y-m-d H:i:s');
            }

            if (in_array('idxed', $this->columns)) {
                $data['idxed'] = 0;
            }

            $data = $this->convertInput($data);

            if (!empty($data)) {
                $this->update($data, $conditions);

                $this->triggerOnUpdate($conditions);

                if (method_exists($this, 'afterUpdate')) {
                    $this->afterUpdate($conditions);
                }

                return true;
            } else {
                return $saveFile;
            }
        } else {
            return false;
        }
    }


    private function uploadHandler($id)
    {
        $this->load->library('Fileupload');
        return $this->fileupload->saveImage($this->table . '/' . $id);
    }


    public function remove($conditions = [])
    {
        $ids = ['id', 'uuid'];

        if (empty($conditions)) {
            foreach ($ids as $i) {
                $id = (int) $this->input->post_stream('id');
                if ($id > 0) {
                    $conditions[$i] = $id;
                    break;
                }
            }
        }

        $this->update(['status' => -1], $conditions);

        return true;
    }


    // Get list
    public function getList($wheres = [], $options = [])
    {
        $result = [];

        if (empty($options) && !empty($this->options)) {
            $options = $this->options;
        }


        /* START CACHE QUERY */
        $this->db->start_cache();

        // Selector
        if (!empty($options['selector'])) {
            $this->db->select($options['selector'], false);
        }

        if (!empty($options['joins']))
        {
            foreach ($options['joins'] as $join) {
                call_user_func_array(array($this->db, 'join'), $join);
            }
        }

        // Group by
        if (!empty($options['group_by'])) {
            foreach ($options['group_by'] as $key) {
                $this->db->group_by($key);
            }
        }
        
        // Order by
        if (!empty($options['order_by'])) {
            foreach ($options['order_by'] as $key => $value) {
                $this->db->order_by($key, $value);
            }
        } else {
            // Sort default
            $this->db->order_by($this->table . '.id', 'DESC');
        }

        // Where
        if (!empty($wheres)) {
            foreach ($wheres as $key => $value) {

                if (is_null($value) && preg_match('#^([a-z0-9_]+)$#', $key) > 0) {
                    $key = 0;
                    $value = [$key, null, false];
                }

                if (is_numeric($key)) {
                    call_user_func_array(array($this->db, 'where'), $value);
                } else {
                    if (is_array($value)) {
                        if (!empty($value)) {
                            $this->db->where_in($key, $value);
                        }
                    } elseif (is_scalar($value)) {
                        $this->db->where($key, $value);
                    }
                }
            }
        }

        if (!empty($options['filter'])) {
            $filters = $this->convertInput($options['filter']);
            $this->db->where($filters);
        }

        if (empty($wheres) && empty($options['filter'])) {
            $this->db->where($this->table . '.status >= ', 0);
        }

        if (!empty($options['since'])) {
            $this->db->where($this->table . '.uuid >', $options['since']);
        }
        

        // Filter
        $this->dataFilters();

        $total = $this->db->count_all_results($this->table);

        /* STOP CACHE QUERY */
        $this->db->stop_cache();


        /* PAGING */
        if (!empty($options['limit']) && $options['limit'] > 0 && $options['limit'] <= 100) {
            $limit = (int) $options['limit'];
        } else {
            $limit = $this->limit;
        }

        if (!empty($options['offset'])) {
            $offset = (int) $options['offset'];
        } else {
            $offset = 0;
        }

        if (!empty($options['page']) && $options['page'] > 0) {
            $page = (int) $options['page'];
            $offset = ($page - 1) * $limit;
        } else {
            $page = ceil(($offset+1)/$limit);
        }

        if (isset($options['getAll'])) {
            $result = $this->db->get($this->table)->result();
        } else {
            $this->db->limit($limit, $offset);

            $result = [
                'result' => true,
                'code' => 200,
                'paging' => [
                    'total' => $total,
                    'limit' => $limit,
                    'offset' => $offset,
                    'page' => $page,
                    'pages' => ceil($total/$limit)
                ],
                'records' => $this->db->get($this->table)->result()
            ];
        }


        /* FLUSH CACHE QUERY */
        $this->db->flush_cache();

        if (!isset($options['getAll']) && $result['paging']['total'] > 0 && method_exists($this, 'getNested')) {
            $result['records'] = $this->getNested($result['records'], $options);
        }


        return $result;
    }


    // Get list all
    public function getListAll($wheres = [], $options = [])
    {
        $options['getAll'] = true;
        
        return $this->getList($wheres, $options);
    }


    public function getAllActive()
    {
        return $this->getList(['status' => 1], ['getAll' => true]);
    }


    // Data filter
    public function dataFilters($filters = null)
    {
        if (empty($filters)) {
            $filters = $this->input->post_stream('filters');
        }

        if (!empty($filters)) {
            $this->db->where($filters);
        }
    }


    // Get list by IDs
    public function getListByIDs($ids)
    {
        if (is_string($ids)) {
            $ids = explode(',', $ids);
        }
        
        if (empty($ids)) {
            return [];
        }

        $result = $this->db
                        ->where_in('id', $ids)
                        ->get($this->table)
                        ->result();

        if ($result) {
            $getNested = $this->getNested([$result]);
            $result = $getNested[0];
        }

        return $result;
    }


    // Get list by field value
    public function getDetailByField($field, $value, $selector = '', $escape = true)
    {
        if (empty($selector)) {
            $selector = '*';
        }
        $result = $this->db
                        ->select($selector, $escape)
                        ->where($field, $value)
                        ->get($this->table)
                        ->row();

        if ($result && method_exists($this, 'getNested')) {
            $getNested = $this->getNested([$result]);
            $result = $getNested[0];
        }

        return $result;
    }


    public function getDetailByCriteria($wheres = [], $selector = '*', $convertOutput = FALSE)
    {
        $result = $this->db
            ->select($selector)
            ->where($wheres)
            ->get($this->table)
            ->row();

        if ($result && $convertOutput && method_exists($this, 'getNested')) {
            $getNested = $this->getNested([$result]);
            $result = $getNested[0];
        }

        return $result;
    }


    // Get detail
    public function getDetail($id, $selector = '*', $escape = true)
    {
        return $this->getDetailByField('id', $id, $selector, $escape);
    }
	
	
	public function getDataBySelectors($selectors, $data)
	{
		if (is_null($selectors)) {
			$result = $data;
		} else {
			$result = new stdClass();
			$selectors = explode(',', $selectors);
			foreach ($selectors as $item) {
				$item = explode(' as ', $item);
				$field = trim(empty($item[1]) ? $item[0] : $item[1]);
				if (isset($data->$field)) {
					$result->$field = $data->$field;
				}
			}
		}
		
		return $result;
	}


    public function triggerOnUpdate($wheres)
    {
        if (method_exists($this, 'deleteCacheOnUpdate')) {
            $this->deleteCacheOnUpdate($wheres);
        }
    }


    public function getDetailCache($field, $value, $selector = '*', $escape = true)
    {
        $this->load->driver('cache');
        $cacheKey = $this->table .':' . $field . '-' . $value . ':' . md5($selector);
        $data = $this->cache->redis->get($cacheKey);
        if ($data === FALSE) {
            $data = is_array($value) ? $this->getListByIDs($value) : $this->getDetailByField($field, $value, $selector, $escape);
            $this->cache->redis->save($cacheKey, $data, 86400);
        }

        return $data;
    }


    public function getDetailRawCache($field, $value = null, $selector = '*', $escape = true)
    {
        $this->load->driver('cache');
        $cacheKey = $this->table . ':' . $field . '-' . $value . ':raw:' . md5($selector);
        $data = $this->cache->redis->get($cacheKey);
        if ($data === FALSE) {
            $data = $this->db
                ->select($selector, $escape)
                ->where($field, $value)
                ->get($this->table)
                ->row();
            $this->cache->redis->save($cacheKey, $data, 86400);
        }

        return $data;
    }
}
