<?php (defined('BASEPATH')) OR exit('No direct script access allowed');


class MY_Input extends CI_Input {

	private $inputData = null;


	public function post_stream($index = null, $xss_clean = true)
	{
		if (is_null($this->inputData)) {
			$inputs = $this->post(null, $xss_clean);
			if (empty($inputs)) {
				$inputs = json_decode($this->raw_input_stream, true);
				if (empty($inputs)) {
					parse_str($this->raw_input_stream, $inputs);
				}
			}
			$this->inputData = $inputs;
		}
		
		if (!is_null($index)) {
			return isset($this->inputData[$index]) ? $this->inputData[$index] : null;
		} else {
			return $this->inputData;
		}
	}
}