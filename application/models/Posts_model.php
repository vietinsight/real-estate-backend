<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Posts_model extends MY_Model {

    public $table = 'post'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update

    public $selector = '*';

    protected $fieldMapping = [
        'type' => [
            1 => 'News',
            2 => 'Event'
        ],
        'status' => [
            -1 => 'Delete',
            0 => 'InActive',
            1 => 'Active'
        ]
    ];


    public function getNested($records, $options = [])
    {
        $this->load->model('Users');
        $this->load->model('Likes');
        $userId = $this->auth->userId();

        foreach ($records as $idx => $record) {
            $record = $this->convertOutput($record);

            $user = $this->Users->getDetailCache('id', $record->user_id, 'id as __id, uuid as id, name, phone, email, type, avatar');
            if ($user) {
                $record->user = $user;
            }

            unset($record->user_id);
            $records[$idx] = $record;
        }

        return $records;
    }
}