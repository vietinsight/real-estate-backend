<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class ShortlistFolders_model extends MY_Model {

    public $table = 'shortlist_folder'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update

    public $selector = 'id, name, user_id, time_created, status';

    protected $fieldMapping = [
        'status' => [
            0 => 'InActive',
            1 => 'Active'
        ]
    ];

    protected $indexes = [
        'name|user_id'
    ];

    protected $fieldValidation = [
        'name' => 'required|trim'
    ];


    public function getNested($records, $options = [])
    {
        foreach ($records as $idx => $row) {
            unset($row->user_id);
            $row->total_listing = $this->db->where('shortlist_folder', $row->id)->count_all_results('like');
            $row->photo = 'http://lorempixel.com/g/400/400/';
            
            $records[$idx] = $this->convertOutput($row);
        }

        return $records;
    }
}