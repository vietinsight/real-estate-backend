<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Users_model extends MY_Model
{

    public $table = 'user'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update

    public $selector = 'id as __id, uuid as id, email, phone, title, name, cea_license_no, cea_saleperson_no, birthday, gender, type, avatar, introduce, balance, specializations, regions_covered, hdb_estates_covered, watermark, status';


    protected $fieldMapping = [
        'title' => [
            0 => 'None',
            1 => 'Mr',
            2 => 'Mrs',
            3 => 'Ms',
            4 => 'Master',
            5 => 'Maid',
            6 => 'Madam'
        ],
        'gender' => [
            0 => 'None',
            1 => 'Male',
            2 => 'Female'
        ],
        'type' => [
            1 => 'Admin',
            2 => 'Mod',
            3 => 'Agent',
            4 => 'Seller',
            5 => 'Buyer'
        ],
        'specializations' => 'Array',
        'regions_covered' => 'Array',
        'hdb_estates_covered' => 'Array',
        'wishlist' => 'Object',
        'status' => [
            -1 => 'Trash',
            0 => 'InActive',
            1 => 'Active'
        ]
    ];

    protected $indexes = [
        'email'
    ];

    protected $fieldValidation = [
        'email' => 'required|trim|valid_email',
        'password' => 'required|min_length[6]',
        'phone' => 'trim|min_length[8]|max_length[15]'
    ];

    protected $groupValidation = [
        'signIn' => [
            'email',
            'password'
        ]
    ];


    public function create($data = [], $extendData = [])
    {
        $extendData = $this->extendData($data);
        if (isset($data['email'])) {
            $data['email'] = strtolower($data['email']);
        }

        $res = parent::create($data, $extendData);

        if (is_int($res)) {
            $this->load->model('Emails');
            if (!is_null($this->auth->getHeader('X-API-Key')) OR !empty($extendData['social'])) {
                $this->Emails->user_register($res);
            }
        }

        return $res;
    }


    public function edit($data = [], $extendData = [])
    {
        $inputs = $this->input->post_stream();
        unset($inputs['email']);

        if (!empty($inputs['password'])) {
            $userId   = $this->input->post('id');
            $password = $this->db->escape($inputs['password']);

            $checkPassword = $this->db->where('id', $userId)->where('`password` = sha1(concat(' . $password . ',salt))', NULL, FALSE)->count_all_results($this->table);

            if ($checkPassword === 0) {
                return FALSE;
            }
        }

        $extendData = $this->extendData($extendData);

        return parent::edit($inputs, $extendData);
    }


    private function extendData($inputs = [])
    {
        if (isset($inputs['email'])) {
            $data['email'] = strtolower($inputs['email']);
        }

        if (!empty($inputs['password'])) {
            $salt               = randomString(6);
            $inputs['salt']     = $salt;
            $inputs['password'] = sha1($inputs['password'] . $salt);
        }

        return $inputs;
    }


    public function getNested($records, $options = [])
    {
        foreach ($records as $idx => $row) {
            $row         = $this->convertOutput($row);
            $row->avatar = $this->getAvatar($row);
            if (isset($row->balance)) {
                $row->balance = (float) $row->balance;
            }

            if (isset($row->specializations) && $row->type === 'Agent') {
                $row->values = [
                    'specializations' => $this->listSpecializations($row->specializations)
                ];
            }
            $records[$idx] = $row;
        }

        return $records;
    }


    public function getAvatar($user, $thumbnail = TRUE)
    {
        $userId = isset($user->__id) ? $user->__id : $user->id;
        $email  = !empty($user->email) ? $user->email : 'info@smartproperty.online';

        if (empty($user->avatar) OR strpos($user->avatar, 'www.gravatar.com') !== FALSE) {
            //return 'https://www.gravatar.com/avatar/' . md5($email);
            return site_url('files/cdn/icon_user.png');
        }
        else {
            return site_url('files/user/' . $userId . '/' . ($thumbnail ? 'thumbnail/' : '') . basename($user->avatar));
        }
    }


    public function setWishlist($data, $userId = NULL)
    {
        if (empty($data)) {
            $data = NULL;
        }
        else {
            $this->load->model('Listings');
            $specialFields = [];
//            $fields        = array_merge($this->Listings->getColumns(), $specialFields);
            if (!empty($data)) {
                foreach ($data as $k => $v) {
                    if (is_array($v)) {
                        $vTemp = [];
                        $j = 0;
                        foreach ($v as $_k => $_v) {
                            if (!empty($_v)) {
                                if (is_numeric($_k)) {
                                    $_k = $j;
                                    $j++;
                                }
                                $vTemp[$_k] = $_v;
                            }
                        }
                        if (empty($vTemp)) {
                            unset($data[$k]);
                        } else {
                            $data[$k] = $vTemp;
                        }
                    }
                }
            }

            if (empty($data)) {
                $data = NULL;
            }
            else {
                $data = json_encode($this->Listings->convertInput($data));
            }
        }

        if (is_null($userId)) {
            $userId = $this->auth->userId();
        }

        $this->update(['wishlist' => $data], ['id' => $userId]);

        return TRUE;
    }


    public function getWishlist($userId = NULL, $user = NULL)
    {
        if (is_null($user)) {
            if (is_null($userId)) {
                $userId = $this->auth->userId();
            }
            $user = $this->getDetailRawCache('id', $userId, 'wishlist');
        }

        if (empty($user->wishlist)) {
            return NULL;
        } else {
            $this->load->model('Listings');

            $data = $this->Listings->convertOutput(json_decode($user->wishlist));

            $values = new stdClass();

            $client = Elasticsearch\ClientBuilder::create()->build();
            $config = $this->config->load('elastic_search', true);

            if (!empty($data->district)) {
                $esParams = [
                    'index' => $config['index'],
                    'type' => 'district',
                    'body' => [
                        'query' => [
                            'constant_score' => [
                                'filter' => [
                                    'terms' => [
                                        'id' => $data->district
                                    ]
                                ]
                            ]
                        ]
                    ]
                ];
                $res = $client->search($esParams);
                $districts = array_map(function ($o){
                    return [
                        'id' => $o['_source']['id'],
                        'code' => $o['_source']['code'],
                        'name' => $o['_source']['name'],
                        'no' => $o['_source']['no']
                    ];
                }, $res['hits']['hits']);
                $values->districts = $districts;
            }

            $data->values = $values;

            return $data;
        }
    }


    public function listSpecializations($values = [], $raw = FALSE)
    {
        $list = [
            'commercial' => 'Commercial Agents',
            'hdb' => 'HDB Sales/Rental',
            'industrial' => 'Industrial Featured Agents',
            'landed' => 'Landed Featured Agents',
            'luxury' => 'Luxury Property Featured Agents',
            'condo' => 'Private Condo Sales/Rental'
        ];

        return $raw ? $list : $this->getListIcons($list, $values);
    }


    private function getListIcons($values, $selecteds = NULL)
    {
        $data = [];
        $path = 'files/cdn/specialization/';

        foreach ($values as $key => $value) {
            $row = [
                'name' => $value,
                'value' => $key,
                'icon_status' => [
                    'active' => site_url($path . $key . '-active.png'),
                    'inactive' => site_url($path . $key . '.png')
                ]
            ];

            if (!empty($selecteds)) {
                $selected        = in_array($key, $selecteds);
                $row['selected'] = $selected;
            }
            else {
                $row['selected'] = FALSE;
            }

            $data[] = $row;
        }

        if (is_null($selecteds)) {
            return [
                'result' => TRUE,
                'version' => 1.0,
                'data' => $data
            ];
        }
        else {
            return $data;
        }
    }


    public function login()
    {
        $inputs   = $this->input->post_stream();
        $username = isset($inputs['username']) ? $inputs['username'] : '';
        $password = isset($inputs['password']) ? $this->db->escape($inputs['password']) : '';

        if (empty($username) OR empty($password)) {
            return FALSE;
        }

        $user = $this->db->where('email', strtolower($username))->where('`password` = sha1(concat(' . $password . ',`salt`))', NULL, FALSE)->get($this->table)->row();

        if (!is_null($user)) {
            if (isset($this->session)) {
                $this->session->set_userdata('userId', $user->id);
            }

            return $user;
        }

        return FALSE;
    }


    public function logout()
    {
        if (isset($this->session)) {
            $this->session->unset_userdata('userId');
        }

        return TRUE;
    }


    public function deleteCacheOnUpdate($wheres)
    {
        $row = $this->getDetailByCriteria($wheres);
        $fields = ['id', 'uuid', 'email'];
        $this->load->library('xRedis');
        foreach ($fields as $field) {
            $keyCache = $this->table . ':' . $field . '-' . $row->{$field} . ':*';
            $this->xredis->delete($keyCache);
        }
    }
}