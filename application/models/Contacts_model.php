<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Contacts_model extends MY_Model {

    public $table = 'contact'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update

    public $selector = 'id, name, email, phone, message, subscribe, time_created, status';

    protected $fieldMapping = [
        'subscribe' => [
            0 => 'No',
            1 => 'Yes'
        ],
        'status' => [
            0 => 'Pending',
            1 => 'Accept'
        ]
    ];
}