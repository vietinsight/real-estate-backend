<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Report_model extends CI_Model {

    private $userId;

    function __construct()
    {
        parent::__construct();

        $this->userId = $this->auth->userId();
    }


    public function listing()
    {
        $data = [];
        $this->db->start_cache();
        $this->db->where('user_id', $this->userId);
        $this->db->stop_cache();

        // BUYER
        $data['short_listed'] = $this->db->where('shortlist', 1)->count_all_results('like');
        $data['viewed'] = (int) $this->db->select('IFNULL(SUM(views), 0) as views', false)->get('listing')->row()->views;
        $data['alert_set'] = 0; // TODO
        $data['wish_list'] = 0; // TODO

        // AGENT
        $data['published'] = 0;
        $data['draft'] = $this->db->where('table', 'listing')->count_all_results('draft');
        $data['expired'] = 0;
        $data['sold'] = $this->db->where('status', 2)->count_all_results('listing');
        $data['active'] = $this->db->where('status', 1)->count_all_results('listing');
        $data['inactive'] = $this->db->where('status', 0)->count_all_results('listing');

        $this->db->flush_cache();

        return $data;
    }


    public function message()
    {
        $this->load->model('Messages');
        $statusData = $this->Messages->getMappingFields('status');

        $data = [];
        $this->db->start_cache();
        $this->db->where('(`user_id` = ' . $this->userId . ' OR `receiver_id` = ' . $this->userId . ')', NULL, FALSE);
        $this->db->stop_cache();

        $data['new'] = 0;
        $data['read'] = 0;

        foreach ($statusData as $v => $status) {
            $data[$status] = $this->db->where('status', $v)->count_all_results($this->Messages->table);
        }

        $this->db->flush_cache();

        return $data;
    }


    public function activity()
    {
        $data = [
            'comment' => $this->db->where('user_id', $this->userId)->count_all_results('comment'),
            'post' => 0,
            'review' => 0,

            'listing_view' => $this->db->select('IFNULL(SUM(views), 0) as views', false)
                                        ->where('user_id', $this->userId)
                                        ->get('listing')
                                        ->row()
                                        ->views,
            'profile_view' => $this->db->select('IFNULL(SUM(views), 0) as views', false)
                                        ->where('id', $this->userId)
                                        ->get('user')
                                        ->row()
                                        ->views
        ];

        return $data;
    }


    public function appointment()
    {
        $this->db->start_cache();
        $this->db->from('appointment');
        $this->db->where('user_id', $this->userId);
        $this->db->stop_cache();

        $data = [
            'total' => $this->db->count_all_results(),
            'new' => $this->db->where('status', 0)->count_all_results()
        ];

        $this->db->flush_cache();

        return $data;
    }
}