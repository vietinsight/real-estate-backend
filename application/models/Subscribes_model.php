<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Subscribes_model extends MY_Model {

    public $table = 'subscribe'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update


    public function buildLink($email, $status)
    {
        $action = $status ? 'subscribe' : 'unsubscribe';
        $time = time();

        return site_url('/lnk/' . $action . '/' . urlencode($email) . '/' . $time . '/' . genToken($action, $email, $time));
    }


    public function handle($email, $time, $token, $status)
    {
        $email = urldecode($email);
        $time = (int) $time;
        $action = $status ? 'subscribe' : 'unsubscribe';

        if ($token === genToken($action, $email, $time)) {
            $chk = $this->getDetailByField('email', $email);
            if (is_null($chk)) {
                $data = [
                    'email' => $email,
                    'status' => 0
                ];
                parent::create($data);
            } else {
                parent::edit(['status' => (int) $status], ['id' => $chk->id]);
            }
            return true;
        } else {
            return false;
        }
    }
}