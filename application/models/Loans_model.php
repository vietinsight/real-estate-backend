<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Loans_model extends MY_Model {

    public $table = 'loan'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update

    public $selector = '*';

    protected $fieldMapping = [
        'mores' => 'Object',
        'status' => [
            -1 => 'Archive',
            0 => 'InActive',
            1 => 'Active'
        ]
    ];

    protected $indexes = [
        'name'
    ];

    protected $fieldValidation = [
        'rate_type' => 'required',
        'lock_in' => 'required',
        'interest_rate' => 'required'
    ];


    public function calculator($inputs)
    {
        $fields = ['present_value', 'rate_per_period', 'number_of_periods', 'interest_rate', 'tenure', 'margin_of_finance'];
        foreach ($fields as $field) {
            ${$field} = empty($inputs[$field]) ? 0 : (float) $inputs[$field];
        }
        $payment = 0;

        if ($present_value > 0) {
            if ($interest_rate > 0 && $tenure > 0) {
                $rate_per_period = $interest_rate/12;
                $number_of_periods = $tenure*12;
            }
            $rate_per_period = $rate_per_period/100;

            if ($number_of_periods > 0) {
                if ($margin_of_finance > 0 && $margin_of_finance < 100) {
                    $present_value *= $margin_of_finance/100;
                }
                $payment = $rate_per_period * $present_value / (1-pow(1+$rate_per_period, -$number_of_periods));
                $payment = round($payment, 2);
            }
        }

        return $payment;
    }


    public function getNested($records, $options = [])
    {
        foreach ($records as $idx => $record) {
            $record->logo = site_url('files/cdn/loans_logo_hsbc.png');
            $records[$idx] = $this->convertOutput($record);
        }

        return $records;
    }
}