<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Emails_model extends MY_Model {

    public $table = 'email'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update

    protected $indexes = [];


    public function send($email, $title, $body, $images = [])
    {
    	$this->load->library('Mailer');
        $this->mailer->setTitle($title);
        $this->mailer->add($email);
        $this->mailer->setBody($body);

        if (!empty($images)) {
            foreach ($images as $name => $path) {
                $this->mailer->AddEmbeddedImage(APPPATH . 'views/emails/images/' . $path, $name);
            }
        }

        $result = $this->mailer->send();

        // var_dump($result);
        // echo '<br />';
        // echo $this->mailer->print_debugger();
    }


    public function user_register($user)
    {
        if (is_numeric($user)) {
            $this->load->model('Users');
            $user = $this->Users->getDetail($user);
        }
        if (!is_null($user) && is_object($user)) {
            $time = time();
            $token = genToken('activate', $user->email, $time);
            $link = site_url('lnk/users/activate/' . urlencode($user->email) . '/' . $time . '/' . $token);
            $gets = $this->input->get();
            if (!empty($gets)) {
                $link .= '?' . http_build_query($gets);
            }

            $this->load->model('Subscribes');
            $config = $this->config->load('common', true);
            $data = (array) $user;
            $data['link'] = $link;
            $data['web_url'] = $config['web_url'];
            $data['web_term_privacy'] = $config['web_url'] . $config['web_uri_term_privacy'];
            $data['web_unsubscribe'] = $this->Subscribes->buildLink($user->email, false);
            $content = $this->load->view('emails/activation', $data, true);

            $images = [
                'logo' => 'logo.png',
                'banner' => 'banner.png',
                'app_store' => 'app-store-label.png',
                'google_play' => 'google-play-label.png'
            ];

            return $this->send($user->email, '[Smartproperty] Activation email', $content, $images);
        } else {
            return false;
        }
    }

    public function set_time($email){

        $content = 'A user has book an appointment with you.';

        $images = [
            'logo' => 'logo.png',
            'banner' => 'banner.png',
            'app_store' => 'app-store-label.png',
            'google_play' => 'google-play-label.png'
        ];

        return $this->send($email, '[Smartproperty] Notification Email', $content);
    }


    public function forgetPassword($user)
    {
        $email = $user->email;
        $hash = (string) $user->access_token;
        if ($hash === '') {
            $hash = '0';
        }
        $time = time();
        $token = genToken('forgetPassword', $email, $time, $hash);
        $link = site_url('lnk/forget-password/' . urlencode($email) . '/' . $time . '/' . $hash . '/' . $token);
        $content = 'Click this link <a href="' . $link . '">' . $link . '</a> to confirm that you want to reset your password. An email will be sent to you with new password.';

        $images = [
            'logo' => 'logo.png',
            'banner' => 'banner.png',
            'app_store' => 'app-store-label.png',
            'google_play' => 'google-play-label.png'
        ];

        return $this->send($email, '[Smartproperty] Forget password', $content);
    }


    public function resetPassword($user)
    {
        $password = randomString(8);
        $email = $user->email;
        $this->db
                ->where('id', $user->id)
                ->set('password', 'sha1(concat("' . $password . '", `salt`))', false)
                ->set('access_token', 'UUID()', false)
                ->update('user');

        $content = 'This is your new password: ' . $password;

        return $this->send($email, '[Smartproperty] New password', $content);
    }
}