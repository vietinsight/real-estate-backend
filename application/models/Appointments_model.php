<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Appointments_model extends MY_Model
{

    public $table = 'appointment'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update

    public $selector = 'appointment.id, message, booking_time, duration, appointment.address, appointment.status, user_id, partner_id';

    protected $fieldMapping = [
        'status' => [
            -1 => 'Cancel',
            0 => 'Pending',
            1 => 'Accept',
            2 => 'Reject'
        ]
    ];

    protected $fieldValidation = [
        'partner_id' => 'required',
        'booking_time' => 'required'
    ];


    public function create($data = [], $extendData = [])
    {
        $wheres = [];
        if ($data['partner_id'] == $data['user_id']) {
            return [
                [
                    'code' => 403,
                    'message' => 'You can not booking yourself'
                ]
            ];
        }
        $wheres['user_id']    = $data['user_id'];
        $wheres['partner_id'] = $data['partner_id'];
        if (!empty($data['listing_id'])) {
            $wheres['listing_id'] = $data['listing_id'];
        }
        $wheres['status'] = empty($data['status']) ? 0 : (int) $data['status'];
        $exist            = $this->get($wheres);
        if ($exist) {
            return [
                [
                    'code' => 409,
                    'message' => 'You have already a same'
                ]
            ];
        }

        return parent::create($data, $extendData);
    }


    public function setFreeTime($data, $listingId = NULL)
    {
        $userId    = $this->auth->userId();
        $timelines = (!empty($data['timelines']) && is_array($data['timelines'])) ? $data['timelines'] : NULL;
        if (!is_null($listingId)) {
            $this->load->model('Listings');
            $field     = is_numeric($listingId) ? 'id' : 'uuid';
            $listing   = $this->Listings->getDetailRawCache($field, $listingId, 'id, user_id');
            $listingId = $listing ? $listing->id : NULL;

            if ($userId != $listing->user_id) {
                return FALSE;
            }
        }

        $timelineData = [];

        if (!is_null($timelines)) {
            foreach ($timelines as $key => $timeline) {
                $date  = (!empty($timeline['date']) && preg_match('#(\d{4})-(\d{2})-(\d{2})#', $timeline['date'])) ? $timeline['date'] : NULL;
                $times = [];
                if (!empty($timeline['times']) && is_array($timeline['times'])) {
                    foreach ($timeline['times'] as $j => $time) {
                        $from = empty($time['from']) ? '00:00' : $time['from'];
                        $to   = empty($time['to']) ? '00:00' : $time['to'];
                        if (is_numeric($from)) {
                            $from .= ':00';
                        }
                        if (is_numeric($to)) {
                            $to .= ':00';
                        }
                        $times[] = [
                            $from,
                            $to
                        ];
                    }
                }

                if (!empty($times) && !is_null($date)) {
                    $timelineData[] = [
                        'date' => $date,
                        'times' => $times
                    ];
                }
            }

            $wheres = [
                'user_id' => $userId
            ];
            if (!is_null($listingId)) {
                $wheres['listing_id'] = $listingId;
            }

            $chk       = $this->db->where($wheres)->get('timeline')->result();
            $dataTimes = ['times' => json_encode($timelineData)];
            if (empty($chk)) {
                $insertData = array_merge($wheres, $dataTimes);
                $this->db->insert('timeline', $insertData);
            }
            else {
                $this->db->where($wheres)->update('timeline', $dataTimes);
            }
        }

        return !empty($timelineData);
    }


    public function getTimeLine($listingId = NULL)
    {
        $timelines   = [];
        $timesByDate = [];
        $appointmentsByDates = [];

        $wheres= [
            'listing_id' => $listingId
        ];
        $resultTimeLine = $this->db->where($wheres)->order_by('date', 'asc')->get('timeline')->row();
        if ($resultTimeLine) {
            $resTimeLines = json_decode($resultTimeLine->times);
            if ($resTimeLines) {
                foreach ($resTimeLines as $t) {
                    $timesByDate[$t->date] = $t->times;
                }
            }
        }

        $next2Days = 14;

        $appointments = $this->db
                                ->select('booking_time, duration')
                                ->where([
                                    'listing_id' => $listingId,
                                    'booking_time >=' => date('Y-m-d'),
                                    'booking_time <=' => date('Y-m-d', strtotime('+' . $next2Days . 'days'))
                                ])
                                ->order_by('booking_time')
                                ->get($this->table)
                                ->result();

        foreach ($appointments as $item) {
            $from = strtotime($item->booking_time);
            $duration = (empty($item->duration) ? 30 : (int) $item->duration) * 60;
            $date = date('Y-m-d', strtotime($item->booking_time));
            $appointmentsByDates[$date][] = [
                'from' => $from,
                'to' => $from + $duration
            ];
        }

        for ($i = 0; $i < $next2Days; $i++) {
            $date = date('Y-m-d', strtotime('+' . $i . 'days'));
            if (isset($timesByDate[$date])) {
                $times = $timesByDate[$date];
                if (empty($times)) {
                    $times = NULL;
                }
            }
            else {
                $times = NULL;
            }

            if (is_null($times)) {
                $times[] = [
                    'from' => '09:00',
                    'to' => '20:00'
                ];
            }
            else {
                foreach ($times as $k => $v) {
                    $times[$k] = [
                        'from' => $v[0],
                        'to' => $v[1]
                    ];
                }
            }

            $tempTimes = [];
            if (!empty($appointmentsByDates[$date])) {
                $appointmentsByDate = $appointmentsByDates[$date];

                usort($appointmentsByDate, function($x, $y){
                    return $x['from'] - $y['from'];
                });

                foreach ($times as $k => $t) {
                    $from = strtotime($date . ' ' . $t['from']);
                    $to = strtotime($date . ' ' . $t['to']);

                    for ($ii = 0; $ii < count($appointmentsByDate); $ii++) {
                        $appByDate = $appointmentsByDate[$ii];
                        if ($appByDate['from'] < $to) {
                            $first = array_shift($appointmentsByDate);
                            if ($first['from'] <= $from) {
                                if ($first['to'] < $to) {
                                    $tempTimes[] = [$first['to'], $to];
                                }
                            } else {
                                $tempTimes[] = [$from, $first['from']];
                                if ($first['to'] < $to) {
                                    $tempTimes[] = [$first['to'], $to];
                                }
                            }
                        } else {
                            break;
                        }
                    }
                }
            }
            if (empty($tempTimes)) {
                $tempTimes = array_map(function($t){return [$t['from'], $t['to']];}, $times);
            }

            foreach ($tempTimes as $k => $t) {
                $from = is_numeric($t[0]) ? $t[0] : strtotime($t[0]);
                $to = is_numeric($t[1]) ? $t[1] : strtotime($t[1]);
                $tempTimes[$k] = [
                    'from' => date('H:i', $from),
                    'to' => date('H:i', $to)
                ];
            }

            $timelines[] = [
                'date' => $date,
                'times' => $tempTimes
            ];
        }

        return $timelines;
    }


    public function getNested($rows = [], $options = [])
    {
        $this->load->model('Users');
        $this->load->model('Listings');

        foreach ($rows as $key => $row) {
            $row = $this->convertOutput($row);

            if (!empty($row->user_id)) {
                $user = $this->Users->getDetailCache('id', $row->user_id, 'id as __id, uuid as id, name, phone, email, type, avatar');
                if ($user) {
                    $row->sender = $user;
                }
                unset($row->user_id);
            }

            if (!empty($row->partner_id)) {
                $partner = $this->Users->getDetailCache('id', $row->partner_id, 'id as __id, uuid as id, name, phone, email, type, avatar');
                if ($partner) {
                    $row->partner = $partner;
                }
                unset($row->partner_id);
            }

            if (!empty($row->listing_id)) {
                $listing = $this->Listings->getDetailCache('id', $row->listing_id, 'id as __id, uuid as id, name');
                if ($listing) {
                    $row->listing = $listing;
                }
                unset($row->listing_id);
            }

            $row->duration = (int) $row->duration;


            $rows[$key] = $row;
        }

        return $rows;
    }
}