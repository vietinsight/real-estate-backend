<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Messages_model extends MY_Model {

    public $table = 'message'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update

    public $selector = 'id, user_id, receiver_id, content, time_created';

    
    protected $fieldMapping = [
        'status' => [
            0 => 'Unread',
            1 => 'Read',
            2 => 'Archive'
        ]
    ];


    protected $fieldValidation = [
        'receiver_id' => 'required',
        'content' => 'required'
    ];


    public function getNested($records, $options = [])
    {
        $this->load->model('Users');
        $this->load->model('Likes');
        $userId = $this->auth->userId();
        $users = [];
        $isAll = isset($options['group_by']);

        foreach ($records as $idx => $record) {
            $record = $this->convertOutput($record);
            $record->time_ago = 'dummy';
            if ($isAll) {
                $record->unreads = 0;
                $partnerId = intval($record->user_id == $userId ? $record->receiver_id : $record->user_id);
            } else {
                $partnerId = $record->user_id;
            }
            
            if (isset($users[$partnerId])) {
                $user = $users[$partnerId];
            } else {
                $user = $this->Users->getDetailCache('id', $partnerId, 'id as __id, uuid as id, name, type, avatar');
                $users[$partnerId] = $user;
            }

            if (!is_null($user)) {
                $record->sender = $user;
                unset($record->user_id, $record->receiver_id);
            } else {
                $record->sender = null;
            }
            $records[$idx] = $record;
        }

        return $records;
    }
}