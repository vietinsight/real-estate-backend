<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Comments_model extends MY_Model {

    public $table = 'comment'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update

    public $selector = 'id, 0 as parent_id, user_id, listing_id, message, time_created';


    public function getNested($records, $options = [])
    {
        $this->load->model('Users');
        $this->load->model('Listings');

        foreach ($records as $idx => $row) {
            if (!empty($row->user_id)) {
                $user = $this->Users->getDetailCache('id', $row->user_id, 'id as __id, uuid as id, name, type');
                if ($user) {
                    $row->user = $user;
                }
                // unset($row->user_id);
            }

            if (!empty($row->listing_id)) {
                $listing = $this->Listings->getDetailCache('id', $row->listing_id, 'id as __id, uuid as id, name, price, photos, photo');
                if ($listing) {
                    unset($listing->photos);
                    $row->listing = $listing;
                }
                // unset($row->listing_id);
                $row->listing_id = $listing->id; // get GUID
            }

            $records[$idx] = $row;
        }

        return $records;
    }
}