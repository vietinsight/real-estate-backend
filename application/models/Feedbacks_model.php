<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Feedbacks_model extends MY_Model {

    public $table = 'feedback'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update

    public $selector = '*';

    protected $fieldMapping = [
        'status' => [
            -1 => 'Deleted',
            0 => 'Unread',
            1 => 'Read'
        ]
    ];

    protected $fieldValidation = [
        'email' => 'trim|valid_email'
    ];


    public function getNested($records, $options = [])
    {
    	$this->load->model('Listings');
    	foreach ($records as $key => $row) {
    		unset($row->user_id, $row->receiver_id);
    		$records[$key] = $this->convertOutput($row);
    	}

    	return $records;
    }
}