<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Likes_model extends MY_Model {

    public $table = 'like'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update
    public $types = [1 => 'shortlist', 2 => 'subscription', 3 => 'rating'];

    protected $indexes = [
        'user_id|listing_id'
    ];
}