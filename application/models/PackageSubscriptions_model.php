<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class PackageSubscriptions_model extends MY_Model {

    public $table = 'package_subscriptions'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update

    public $selector = '*';

    protected $fieldMapping = [
        'status' => [
            -1 => 'Archive',
            0 => 'InActive',
            1 => 'Active'
        ]
    ];

    public function getNested($records, $options = [])
    {
        $this->load->model('Loans');
        $packages = [];

        foreach ($records as $idx => $record) {
            $packageId = intval($record->package_id);
            if (isset($packages[$packageId])) {
                $package = $packages[$packageId];
            } else {
                $package = $this->Loans->getDetailCache('id', $packageId, 'id, bank_name, bank_icon, package_name, package_rate');
            }
            if (!is_null($package)) {
                $record->package = $package;
                unset($record->package_id);
            } else {
                $record->package = null;
            }
            $records[$idx] = $record;
        }

        return $records;
    }
}