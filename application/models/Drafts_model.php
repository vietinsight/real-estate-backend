<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Drafts_model extends MY_Model {

    public $table = 'draft'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update

    public $selector = '*';

    protected $fieldMapping = [
        'status' => [
            -1 => 'Archive',
            0 => 'InActive',
            1 => 'Active'
        ]
    ];


    public function create($inputs = [], $table = null)
    {
    	if (!empty($inputs)) {
	        $inputs = json_encode($inputs);
	        $userId = $this->auth->userId();

	        $data = [
	            'user_id' => $userId,
	            'table' => $table,
	            'data' => $inputs
	        ];
	        $data['hash'] = sha1(json_encode($data));

	        $chk = $this->db
	        			->select('id')
	        			->where('hash', $data['hash'])
	        			->get($this->table)
	        			->row();

	        if (is_null($chk)) {
	        	$id = parent::create($data);
	        } else {
	        	$id = $chk->id;
	        }

	        return $id;
    	} else {
    		return false;
    	}
    }


    public function edit($inputs = [], $table = null, $id = null)
    {
        $inputs = json_encode($inputs);
        $userId = $this->auth->userId();

        $data = [
            'user_id' => $userId,
            'table' => $table,
            'data' => $inputs
        ];
        $data['hash'] = sha1(json_encode($data));

        parent::edit($data, ['id' => $id]);

        return true;
    }


    public function trash($id)
    {
    	$this->db->delete($this->table, ['id' => $id]);
    }


    public function getList($wheres = [], $options = [])
    {
    	return parent::getList($wheres, $options);
    }


    public function getDetail($id, $selector = '*', $escape = true)
    {
    	return parent::getDetail($id);
    }


    public function getNested($records, $options = [])
    {
    	$this->load->model('Listings');
    	foreach ($records as $key => $row) {
    		$row->data = json_decode($row->data);
    		unset($row->user_id, $row->table);
    		$records[$key] = $this->convertOutput($row);
    	}

    	return $records;
    }
}