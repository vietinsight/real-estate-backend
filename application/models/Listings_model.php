<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Listings_model extends MY_Model {

    public $table = 'listing'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array('rating_count', 'rating_point', 'views'); // ...Or you can set an array with the fields that cannot be filled by insert/update

    public $selector = 'listing.id as __id, listing.uuid as id, listing.name, address, district_id, postal_code, type, property_type, area, unit_type, unit_no, tenure, listing.age, bath_room, bed_room, appointment, transport, amenity, map_lat, map_lon, description, price, price_psf, historical, floor, floor_publish, sun, facilities, amenities, listing.note, listing.photo, listing.photos, new_launch_data, rating_count, rating_point, listing.views, listing.user_id, listing.time_created, listing.time_updated, listing.status';

    protected $intFields = [
                'district_id',
                'area',
                'tenure',
                'age',
                'bath_room',
                'bed_room',
                'amenity',
                'price',
                'price_psf',
                'historical',
                'rating_count',
                'rating_point',
                'views'
            ];

    protected $floatFields = [
                'map_lat',
                'map_lon'
            ];

    
    protected $fieldMapping = [
        'near_by' => 'Object',
        'facilities' => 'Array',
        'amenities' => 'Array',
        'available_units' => 'Object',
        'new_launch_data' => 'Object',
        'photos' => 'Object',
        'type' => [
            1 => 'ForSale',
            2 => 'ForRent',
            3 => 'NewLaunch'
        ],
        'property_type' => [
            1 => 'HDB',
            2 => 'CondoApartment',
            3 => 'ExecutiveCondo',
            4 => 'Landed'
        ],
        'unit_type' => [
            1 => 'Sqm',
            2 => 'Sqft'
        ],
        'tenure' => [
            1 => 'FreeHold',
            2 => '99Years',
            3 => '999Years',
            4 => 'Other'
        ],
        'floor' => [
            1 => 'High',
            2 => 'Medium',
            3 => 'Low'
        ],
        'floor_publish' => [
            1 => 'Yes',
            0 => 'No'
        ],
        'sun' => [
            1 => 'AM',
            2 => 'PM'
        ],
        'status' => [
            -1 => 'Archive',
            0 => 'InActive',
            1 => 'Active',
            2 => 'Sold'
        ]
    ];

    protected $fieldValidation = [
        'address' => 'required',
        'map_lat' => 'required',
        'map_lon' => 'required',
        'price' => 'required'
    ];


    public function create($data = [], $extraData = [])
    {
        if (isset($data['postal_code'])) {
            $data['postal_code'] = trim($data['postal_code']);
        }
        $res = parent::create($data);
        if (is_numeric($res) && !empty($data['postal_code'])) {
            $this->calcPsf($data['postal_code']);
        }

        return $res;
    }


    public function calcPsf($address = false)
    {
        $table = $this->table . '_psf';

        if ($address === false) {
            $this->db->truncate($table);
            $this->db->query('INSERT INTO ' . $table . '(loc_key, psf) SELECT postal_code, AVG(`price_psf`) as psf FROM ' . $this->table . ' WHERE postal_code IS NOT NULL AND postal_code != "" GROUP BY postal_code');
        } else {
            $res = $this->db->where('postal_code', $address)->select_avg('price_psf')->get($this->table)->row();
            $psf = (float) $res->price_psf;

            $chk = $this->db->where('loc_key', $address)->count_all_results($table);
            if ($chk === 0) {
                $this->db->insert($table, ['loc_key' => $address, 'psf' => $psf]);
            } else {
                $this->db->where('loc_key', $address)->set('psf', $psf)->update($table);
            }
        }
    }


    public function getNested($records, $options = [])
    {
        $this->load->model('Users');
        $this->load->model('Likes');
        $userId = $this->auth->userId();

        $listingIds = array_filter(array_map(function($o){
            if (isset($o->__id)) {
                return $o->__id;
            }
        }, $records));

        $likes = $this->Likes->getList([
            'user_id' => $userId,
            'listing_id' => $listingIds
        ], ['getAll' => true]);

        $userActions = [];
        if (!empty($likes)) {
            foreach ($likes as $o) {
                $userActions[$o->user_id . '-' . $o->listing_id] = $o;
            }
        }

        foreach ($records as $idx => $record) {
            $record = $this->convertOutput($record);

            if (isset($record->user_id)) {
                $user = $this->Users->getDetailCache('id', $record->user_id, 'id as __id, uuid as id, name, phone, email, type, avatar, cea_name, job_title');
                if ($user) {
                    $record->user = $user;
                }
            }

            if (isset($record->__id)) {
                // ACTIONS
                if (isset($record->user_id)) {
                    $uActions = new stdClass();
                    if (!is_null($userId)) {
                        $likeKey = $userId . '-' . $record->__id;
                        if (isset($userActions[$likeKey])) {
                            $uActions = $userActions[$likeKey];
                        }
                    }

                    foreach ($this->Likes->types as $key => $item) {
                        $value = isset($uActions->{$item}) ? $uActions->{$item} : 0;
                        if ($key === 3) {
                            $value = $value > 0 ? intval($value) : 0;
                        } else {
                            $value = $value === '1' ? true : false;
                        }
                        $record->user_actions[$item] = $value;
                    }
                }

                // PHOTOS
                $photos = [];
                $photoExtras = [];
                $photo = '';
                $path = 'files/' . $this->table . '/' . $record->__id . '/';
                if (!empty($record->photos) && is_array($record->photos)) {
                    $files = glob($path . '*.{jpg,jpeg,gif,png,JPG,JPEG,GIF,PNG}', GLOB_BRACE);

                    $filenames = array_map(function($fn){
                        return basename($fn);
                    }, $files);

                    foreach ($record->photos as $k => $p) {
                        if (!empty($p->url)) {
                            $filename = basename($p->url);
                            if (in_array($filename, $filenames)) {
                                $p->url = site_url($path . $filename);
                                $photos[] = $p;

                                // Extra photos
                                // $x = $p;
                                // $x->url = site_url($path . 'medium/' . $filename);
                                // $photoExtras['medium'][] = $x;

                                // $y = $p;
                                // $y->url = site_url($path . 'thumbnail/' . $filename);
                                // $photoExtras['thumbnail'][] = $y;
                            }
                        }
                    }
                }

                if (property_exists($record,'photos')) {
                    $record->photos = $photos;
                }

                if (property_exists($record,'photo')) {
                    $fName = null;
                    if (empty($record->photo)) {
                        if (!empty($photos)) {
                            $fName = basename($photos[0]->url);
                        }
                    } else {
                        $fName = basename($record->photo);
                    }
                    if (!empty($fName)) {
                        $record->photo = site_url($path . 'medium/' . $fName);
                    }
                }

                // $path = 'files/' . $this->table . '/' . $record->_id . '/';
                // $files = glob($path . '*.{jpg,jpeg,gif,png,JPG,JPEG,GIF,PNG}', GLOB_BRACE);
                // $photos = [];
                // $photo = '';
                // if (!empty($files)) {
                //     foreach ($files as $file) {
                //         if (strpos($file, 'cover_file') === false) {
                //             $photos[] = site_url($file);
                //         } else {
                //             $photo = site_url($file);
                //         }
                //     }
                // }
                // $record->photos = $photos;
                // $record->photo = (!empty($photos) && $photo === '') ? $photos[0] : $photo;

            }

            foreach ($this->intFields as $field) {
                if (isset($record->{$field})) {
                    $record->{$field} = (int) $record->{$field};
                }
            }

            foreach ($this->floatFields as $field) {
                if (isset($record->{$field})) {
                    $record->{$field} = (float) $record->{$field};
                }
            }

            // Infer
            $values = [];
            if (isset($record->price_psf)) {
                $values['premium'] = $record->price_psf > 3000;
            }
            if (isset($record->facilities)) {
                $values['facilities'] = $this->listFacilities($record->facilities);
            }
            if (isset($record->amenities)) {
                $values['amenities'] = $this->listFacilities($record->amenities);
            }

            if (!empty($values)) {
                $record->values = $values;
            }

            unset($record->user_id);
            $records[$idx] = $record;
        }

        return $records;
    }


    public function getListES($wheres = [], $options = [])
    {
        $this->load->library('Esearch');
        $this->esearch->selectType($this->table);
        $result = [];
        $queries = [];

        if (empty($options) && !empty($this->options)) {
            $options = $this->options;
        }

        // Selector
        if (!empty($options['selector'])) {
            
        }


        // Group by
        if (!empty($options['group_by'])) {
            foreach ($options['group_by'] as $key) {
                
            }
        }
        
        // Order by
        if (!empty($options['order_by'])) {
            foreach ($options['order_by'] as $key => $value) {
                
            }
        } else {
            // Sort default
            
        }

        // Where
        if (!empty($wheres)) {
            foreach ($wheres as $key => $value) {
                if (is_array($value)) {
                    if (!empty($value)) {
                        
                    }
                } else {
                    
                }
            }
        }

        if (!empty($options['filter'])) {
            $filters = $this->convertInput($options['filter']);
            
        }

        if (empty($wheres) && empty($options['filter'])) {
            
        }

        if (!empty($options['since'])) {
            
        }
        

        /* PAGING */
        if (!empty($options['limit']) && $options['limit'] > 0 && $options['limit'] <= 100) {
            $limit = (int) $options['limit'];
        } else {
            $limit = $this->limit;
        }

        if (!empty($options['offset'])) {
            $offset = (int) $options['offset'];
        } else {
            $offset = 0;
        }

        if (!empty($options['page']) && $options['page'] > 0) {
            $page = (int) $options['page'];
            $offset = ($page - 1) * $limit;
        } else {
            $page = ceil(($offset+1)/$limit);
        }

        if (isset($options['getAll___'])) {
            // $result = $this->db->get($this->table)->result();
        } else {
            // $this->db->limit($limit, $offset);
            $queries['size'] = $limit;
            if ($offset > 0) {
                $queries['from'] = $offset;
            }

            if (isset($options['body'])) {
                // $queries = [];
                $queries = array_merge_recursive($queries, $options['body']);
            }

            $EsResults = $this->esearch->search($queries);
            $total = $EsResults['hits']['total'];

            $result = [
                'result' => true,
                'paging' => [
                    'total' => $total,
                    'limit' => $limit,
                    'offset' => $offset,
                    'page' => $page,
                    'pages' => ceil($total/$limit)
                ],
                'records' => array_map(function($row){
                    return $row['_source'];
                }, $EsResults['hits']['hits'])
            ];
        }


        if (!isset($options['getAll']) && $result['paging']['total'] > 0 && method_exists($this, 'getNestedES')) {
            $result['records'] = $this->getNestedES($result['records'], true);
        }


        return $result;
    }


    public function getNestedES($records, $dynamic = true)
    {
        if ($dynamic) {
            $this->load->model('Users');
            $this->load->model('Likes');
            $userId = $this->auth->userId();
        }

        foreach ($records as $idx => $record) {
            $record = (object) $record;
            if ($dynamic) {
                $user = $this->Users->getDetailCache('id', $record->user_id, 'id as __id, uuid as id, name, phone, email, type, avatar');
                if ($user) {
                    $record->user = $user;
                }

                if (isset($record->__id)) {
                    if (!is_null($userId)) {
                        $userActions = $this->Likes->get([
                                                    'user_id' => $userId,
                                                    'listing_id' => $record->__id
                                                ]);
                    }
                    foreach ($this->Likes->types as $key => $item) {
                        $value = isset($userActions->{$item}) ? $userActions->{$item} : 0;
                        if ($key === 3) {
                            $value = $value > 0 ? intval($value) : 0;
                        } else {
                            $value = $value === '1' ? true : false;
                        }
                        $record->user_actions[$item] = $value;
                    }
                }

                unset($record->user_id, $record->isProject);

            } else {
                $record = $this->convertOutput($record);

                foreach ($this->intFields as $field) {
                    $record->{$field} = (int) $record->{$field};
                }

                foreach ($this->floatFields as $field) {
                    $record->{$field} = (float) $record->{$field};
                }

                // Infer
                $record->values = [
                    'premium' => $record->price_psf > 3000,
                    'facilities' => $this->listFacilities($record->facilities),
                    'amenities' => $this->listAmenities($record->amenities),
                ];

                // PHOTOS
                $photos = [];
                $photoExtras = [];
                $photo = '';
                $path = 'files/' . $this->table . '/' . $record->__id . '/';
                if (!empty($record->photos) && is_array($record->photos)) {
                    $files = glob($path . '*.{jpg,jpeg,gif,png,JPG,JPEG,GIF,PNG}', GLOB_BRACE);

                    $filenames = array_map(function($fn){
                        return basename($fn);
                    }, $files);

                    foreach ($record->photos as $k => $p) {
                        $filename = urlencode(basename($p->url));
                        if (in_array($filename, $filenames)) {
                            $p->url = site_url($path . $filename);
                            $photos[] = $p;

                            // Extra photos
                            // $x = $p;
                            // $x->url = site_url($path . 'medium/' . $filename);
                            // $photoExtras['medium'][] = $x;

                            // $y = $p;
                            // $y->url = site_url($path . 'thumbnail/' . $filename);
                            // $photoExtras['thumbnail'][] = $y;
                        }
                    }
                    $record->photos = $photos;
                } else {
                    $record->photos = [];
                }

                $record->photos = $photos;
                // $record->photo_extras = $photoExtras;
                $record->photo = (!empty($photos) && $photo === '') ? site_url($path . basename($photos[0]->url)) : site_url($path . urlencode(basename($record->photo)));
            }

            unset($record->data);
            $records[$idx] = $record;
        }

        return $records;
    }


    public function getSimilarNearBy($listingId, $options, $isProject = false)
    {
        $distance = '2km';
        $listing = $this->getDetailRawCache('uuid', $listingId);
        if (!is_null($listing)) {
            $mapLat = (float) $listing->map_lat;
            $mapLon = (float) $listing->map_lon;

            $options['body'] = [
                            'query' => [
                                'filtered' => [
                                    'query' => [
                                        'match' => [
                                            'isProject' => $isProject
                                        ]
                                    ],
                                    'filter' => [
                                        'geo_distance' => [
                                            'distance' => $distance,
                                            'location' => [
                                                'lat' => $mapLat,
                                                'lon' => $mapLon
                                            ]
                                        ]
                                    ]
                                ]
                            ],

                          'sort' => [
                            [
                              '_geo_distance' => [
                                'location' => [
                                  'lat' => $mapLat,
                                  'lon' => $mapLon
                                ],
                                'order' => 'asc',
                                'unit' => 'km', 
                                'distance_type' => 'plane' 
                              ]
                            ]
                          ]
            ];

            if (isset($options['offset'])) {
                $options['offset']++;
            } else {
                $options['offset'] = 1;
            }
            $response = $this->getListES([], $options);
            if (isset($response['paging']['total']) && $response['paging']['total'] > 0) {
                $response['paging']['total']--;
                $response['paging']['offset']--;
            }

            foreach ($response['records'] as $key => $row) {
                $response['records'][$key]->distance = distance($mapLat, $mapLon, $row->map_lat, $row->map_lon, 'm');
            }
            
            $response = ['distance' => $distance] + $response;
        } else {
            $response = [
                'result' => false,
                'reason' => 'item_not_exist',
                'message' => 'This listing id is not exist'
            ];
        }

        return $response;
    }


    public function calculateRating($listingId)
    {
        $this->db->query('UPDATE ' . $this->table . ' as t, (SELECT listing_id, count(*) as count, sum(rating) as point FROM `like` WHERE listing_id = ' . $listingId . ' GROUP BY listing_id) as s SET t.rating_count = s.count, t.rating_point = s.point, t.rating = s.point/s.count, t.idxed = 0 where t.id = ' . $listingId);
    }


    public function getNearBy($lat, $lon)
    {
        $data = [];
        $mrt = ['bus_station', 'subway_station'];

        $types = array_merge($mrt, ['school', 'shopping_mall']);

        $params = [
            'key' => 'AIzaSyC7YSO1uK0JSR0mKe9LTNunY3rIlQQD2io',
            'language' => 'en',
            'location' => floatval($lat) . ',' . floatval($lon),
            'radius' => 500,
            'type' => implode('|', $types)
        ];

        $result = curl('https://maps.googleapis.com/maps/api/place/nearbysearch/json', $params);
        if ($result->status === 'OK') {
            foreach ($result->results as $row) {
            	if (isset($row->geometry->location->lat) && isset($row->geometry->location->lng)) {
            		$row->distance = distance($row->geometry->location->lat, $row->geometry->location->lng, $lat, $lon);
            	} else {
            		$row->distance = 0;
            	}
                $matches = array_intersect($types, $row->types);
                if (!empty($matches)) {
                    $type = array_shift($matches);
                    if (in_array($type, $mrt)) {
                        $data['mrt'][$type][] = $row;
                    } else {
                        $data[$type][] = $row;
                    }
                }
            }
        }
        
        return empty($data) ? new stdClass() : $data;
    }


    public function afterUpdate($wheres)
    {
        $this->db->set('price_psf', 'IF(`area`> 0, `price`/(`area` * IF(`unit_type`=1, 3.2808, 1)), 0)', false);

        $this->db->where($wheres)->update($this->table);
    }


    public function getAvg($field, $wheres = [])
    {
        $this->db->select_avg($field);
        if (!empty($wheres)) {
            $this->db->where($wheres);
        }
        $res = $this->db->get($this->table)->row();

        return $res ? (float) $res->{$field} : 0;
    }


    public function shortlistTotal($userId = null)
    {
        if (is_null($userId)) {
            $userId = $this->auth->userId();
        }

        return $this->db
                    ->where('user_id', $userId)
                    ->where('shortlist', 1)
                    ->count_all_results('like');
    }


    public function listFacilities($values = null, $raw = FALSE)
    {
        $list = [
            'Gym' => 'Gym',
            'Pool' => 'Pool',
            'Tennis' => 'Tennis',
            'PlayGround' => 'Play Ground',
            'Security' => 'Security',

            'Badminton' => 'Racquetball',
            'Balcony' => 'Balcony',
            'BBQ' => 'BBQ',
            'CarParking' => 'Car Park',
            'ClubHouse' => 'Club House',
            'Sauna' => 'Sauna',
            'SuperMarket' => 'Mini Mart',
            
        ];

        return $raw ? $list : $this->getListIcons($list, $values);
    }


    public function listAmenities($values = null, $raw = FALSE)
    {
        $list = [
            'Aircon' => 'Air conditioning',
            'Bathtub' => 'Bathtub',
            'Closet' => 'Closet',
            'DinningRoomFurniture' => 'Dinning Room Furniture',
            'Dishwasher' => 'Dishwasher',
            'Dryer' => 'Dryer',
            'Fridge' => 'Fridge',
            'Sofa' => 'Sofa',
            'Stove' => 'Stove',
            'TV' => 'TV',
            'Washer' => 'Washer',
        ];

        return $raw ? $list : $this->getListIcons($list, $values);
    }


    private function getListIcons($values, $selecteds = null)
    {
        $data = [];
        $path = 'files/cdn/listing_icons/';

        foreach ($values as $key => $value) {
            $row = [
                'name' => $value,
                'value' => $key,
                'icon_status' => [
                    'active' => site_url($path . $key . '.png'),
                    'inactive' => site_url($path . $key . '_inactive.png')
                ]
            ];

            if (!empty($selecteds)) {
                $selected = in_array($key, $selecteds);
                $row['selected'] = $selected;
            } else {
                $row['selected'] = false;
            }

            $data[] = $row;
        }

        if (is_null($selecteds)) {
            return [
                'result' => true,
                'version' => 1.0,
                'data' => $data
            ];
        } else {
            return $data;
        }
    }


    public function deleteCacheOnUpdate($wheres)
    {
        $row = $this->getDetailByCriteria($wheres);
        $fields = ['id', 'uuid'];
        $this->load->library('xRedis');
        foreach ($fields as $field) {
            if (isset($row->{$field})) {
                $keyCache = $this->table . ':' . $field . '-' . $row->{$field} . ':*';
                $this->xredis->delete($keyCache);
            }
        }
    }

}