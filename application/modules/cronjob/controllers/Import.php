<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Import extends MX_Controller {


	function __construct()
	{
		parent::__construct();
		$this->load->library('Auth');
	}


	public function index()
	{
		echo 'cronjob';
	}


	function mapping()
	{
		eSearch('', [], 'DELETE');
		echo 'Delete index</br>';

        $mappings = [
            'mappings' => [
                'listing' => [
                    'properties' => [
                        'location' => ['type' => 'geo_point']
                    ]
                ]
            ]
        ];
        $res = eSearch('', $mappings, 'PUT');

        $this->load->database();
        $tables = ['listing'];
        foreach ($tables as $table) {
        	$this->db->set('idxed', 0)->update($table);
        }

        print_r($res);
	}


	function agent()
	{
		$lines = file('files/import/EStateList.csv');
		foreach ($lines as $idx => $line) {
			$fields = ['name', 'license', 'valid_from', 'valid_to'];
			$data = [];
			$parts = explode(',', $line);
			foreach ($parts as $i => $part) {
				$data[$fields[$i]] = trim($part);
			}
			eSearch('agent/' . ($idx+1), $data, 'PUT');
		}
	}


	function district()
	{
        $data = [];
        $lines = file('files/import/district.csv');
        foreach ($lines as $idx => $r) {
            $j = 1;
            $code = [];
            $name = '';
            foreach (explode('",', $r) as $i) {
                $i = trim(preg_replace('/\s\s+/', ' ', $i), '" ');
                if ($j === 1) {
                    $code = explode(',', $i);
                    foreach ($code as $k => $v) {
                        $code[$k] = trim($v);
                    }
                } else {
                    $name = $i;
                }
                $j++;
            }
            $data[] = [
            	'name' => $name,
            	'code' => $code
            ];
            $id = $idx + 1;
            $esData = [
	    		'id' => $id,
            	'no' => 'District ' . $id,
            	'name' => $name,
            	'code' => $code
        	];
            $res = eSearch('district/' . $id, $esData, 'PUT');
        }

        // echo json_encode($data);
	}


	function projects()
	{
		$lines = file('files/import/projects.csv');
		foreach ($lines as $key => $l) {
			$id = $key + 1;
			$l = trim($l);
			var_dump($l);
			eSearch('project/' . $id, ['name' => $l], 'PUT');
		}
	}

	function hdb()
	{
		$lines = file('files/import/hdb.csv');
		foreach ($lines as $key => $l) {
			$id = $key + 1;
			list($block, $streetName) = explode(',', $l);
			$data = [
				'id' => $id,
				'block' => $block,
				'street_name' => trim($streetName)
			];
			var_dump($data);
			eSearch('hdb/' . $id, $data, 'PUT');
		}
	}


	function es_index_listing()
	{
		$this->load->database();
		$this->output->enable_profiler(TRUE);
		$this->load->model('Listings');
		$table = $this->Listings->table;

		$listings = $this->db
							->select('listing.*, id as __id, uuid as id')
							->where('idxed', 0)
							->limit(200)
							->get($table)
							->result();

		$ids = [];
		if (!empty($listings)) {
			$listings = $this->Listings->getNestedES($listings, false);
		}
		foreach ($listings as $row) {
			$id = $row->__id;
			$row->location = [
				'lat' => $row->map_lat,
				'lon' => $row->map_lon
			];
			$row->isProject = $row->type === 'NewLaunch' ? true : false;
			$row->withPhoto = !empty($row->photos);
			unset($row->uuid, $row->near_by, $row->idxed);
			// var_dump($row);
			$res = eSearch($table . '/' . $id, $row, 'PUT');
			$ids[] = $id;
		}

		if (!empty($ids)) {
			$this->db->where_in('id', $ids)->set('idxed', 1)->update($table);
		}
	}


	function es_truncate($type = null)
	{
		if (!is_null($type)) {
			$type = '/' . $type . '/_search?q=*';
		}
		$type = '';
		$res = eSearch($type, [], 'DELETE');
	}

}
