<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Upload extends MX_Controller {


	function __construct()
	{
		parent::__construct();

        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');

		$this->load->library('Fileupload');
	}


	public function index()
	{
		switch ($this->input->method()) {
			case 'post':
				if ($this->fileupload->handler() === false) {
					echo json_encode([
							'result' => false,
							'status' => 401,
							'files' => []
						]);
				}
			break;

			case 'get':
				echo json_encode([
						'result' => true,
						'data' => $this->fileupload->getListFiles()
					]);
			break;

			case 'delete':
				$result = $this->fileupload->clearDraft();

				$response = [
					'result' => 'true'
				];
				if ($result['files'] > 0) {
					$response['data'] = $result;
				}
		        
				echo json_encode($response);
			break;
		}

	}
	

	public function unique_id()
	{
        $uniqueId = $this->fileupload->getUniqueId();
        
		echo json_encode([
				'result' => $uniqueId ? true : false,
				'data' => $uniqueId
			]);
	}
}
