<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/API_Controller.php';


class Messages extends API_Controller {

    protected $model = 'Messages';
    protected $selector;


    public function receivers_post($receiverId = null)
    {
        $this->auth->checkToken();

    	$this->load->model('Users');
        $error = false;
    	$data = $this->input->post_stream();
    	$userId = $this->auth->userId();
    	if (is_null($userId)) {
    		$error = 'not_user_id';
    	} elseif (is_null($receiverId)) {
            $error = 'not_receiver_id';
        } else {
            $field = preg_match('#^\d+$#', $receiverId) ? 'id' : 'uuid';
            $receiver = $this->Users->getDetailCache($field, $receiverId, 'id');
            if (empty($receiver)) {
                $receiverId = 0;
                $error = 'not_receiver_id';
            } else {
                $receiverId = (int) $receiver->id;
            }
            
            if ($userId === $receiverId) {
                $error = 'your';
            }
        }

        if ($error === false) {
            $data['user_id'] = $userId;
            $data['receiver_id'] = $receiverId;
            $data['channel'] = $this->getChannel($userId, $receiverId, false);

            $res = $this->Model->create($data);

            if (is_int($res) && $res > 0) {

                $this->load->library('Xpusher');
                // $channelGroup = $this->getChannel($userId, $receiverId);
                $sender = $this->Users->getDetailCache('id', $userId, 'id as __id, uuid as id, name, type, avatar');
                $channels = [$this->getChannel($receiverId)];
                if (isset($_GET['with_return'])) {
                    $channels[] = $this->getChannel($userId);
                }
                $this->xpusher->trigger($channels, 'message', [
                                'id' => strval($res),
                                'content' => $data['content'],
                                'time_created' => date('Y-m-d H:i:s'),
                                'time_ago' => '',
                                'sender' => $sender
                            ]);

                $response = [
                    'result' => true,
                    'code' => 201,
                    'data' => [
                        'id' => $res,
                        'channels' => count($channels)
                    ]
                ];
            } else {
                $response = [
                    'result' => true,
                    'code' => 400,
                    'errors' => $res,
                    'message' => 'Bad Request: validation errors, missing or exist data'
                ];
            }
        } else {
            $response = [
                'result' => false,
                'status' => 400,
                'error' => $error,
                'message' => 'Bad Request: validation errors, missing data'
            ];
        }

        $this->set_response($response);
    }


    public function receivers_get($receiverId = null)
    {
        $this->auth->checkToken();

        $wheres = [];
        $userId = $this->auth->userId();
        $options = $this->getOptions();
        if (!empty($receiverId)) {
            $this->load->model('Users');
            $field = is_numeric($receiverId) ? 'id' : 'uuid';
            $receiver = $this->Users->getDetailCache($field, $receiverId);
            $receiverId = is_null($receiver) ? 0 : (int) $receiver->id;
            $wheres['channel'] = $this->getChannel($userId, $receiverId, false);
        } else {
            $wheres['(user_id = "' . $userId . '" OR receiver_id = "' . $userId . '")'] = null;
            $options['group_by'] = ['channel'];
        }
        $options['selector'] = $this->Model->selector;

        $options['order_by']['message.id'] = 'desc';
	
        $response = $this->Model->getList($wheres, $options);
        if (!is_null($receiverId) && !empty($response['records'])) {
            $response['records'] = array_reverse($response['records']);
        }

        $this->set_response($response);
    }


    public function channels_get($receiverId = null)
    {
        $this->auth->checkToken();

        $userId = $this->auth->userId();
        $partnerId = 0;
        if (!is_null($receiverId)) {
            $this->load->model('Users');
            $field = preg_match('#^\d+$#', $receiverId) ? 'id' : 'uuid';
            $receiver = $this->Users->getDetailCache($field, $receiverId);
            if (!is_null($receiver)) {
                $partnerId = (int) $receiver->id;
            }
        }

        if ($userId > 0) {
            if ($partnerId > 0 && $userId !== $partnerId) {
                $channel = $this->getChannel($userId, $partnerId);
                $type = 'group';
            } else {
                $channel = $this->getChannel($userId);
                $type = 'home';
            }
            $response = [
                'result' => true,
                'data' => [
                    'channel' => $channel,
                    'type' => $type
                ]
            ];
        } else {
            $response = [
                'result' => false,
                'status' => 400
            ];
        }

        $this->set_response($response);
    }


    private function getChannel($userId, $partnerId = null, $prefix = true)
    {
        $arr = [$userId];
        if (!empty($partnerId)) {
            $arr[] = $partnerId;
        }
        asort($arr);

        $config = $this->config->load('auth', true);

        if ($prefix) {
            return 'cprivate-' . sha1($userId . $config['secret_key']);
        } else {
            return implode('-', $arr);
        }
    }


    public function auth_get()
    {

    }


    public function configs_get()
    {
        $config = $this->load->config('pusher', true);

        $userId = $this->auth->userId();
        $channel = is_null($userId) ? '' : $this->getChannel($userId);

        $response = [
            'result' => true,
            'code' => 200,
            'data' => [
                'app_id' => $config['app_id'],
                'app_key' => $config['app_key'],
                'options' => $config['options'],
                'channel' => [
                    'private' => $channel,
                    'public' => 'channelSP'
                ],
                'events' => [
                    'receive_message' => 'message'
                ]
            ]
        ];

        $this->set_response($response);
    }


    public function xc_get($userId = null)
    {
        $response = [
            'x' => $this->getChannel($userId)
        ];
        $this->set_response($response);
    }
}
