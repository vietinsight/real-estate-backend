<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/API_Controller.php';


class Promos extends API_Controller {

    protected $model = 'Promos';
    
    public function index_get($code = null)
    {
        $where = [];
        $data = $this->input->get();
        if(!empty($data['id'])){
            $where['id = ' . $data['id']] = null;
        }
        if(!empty($data['code']))
            $where['code = "' . $data['code'] . '"'] = null;

        $where['status = 1'] = null;
        $options = $this->getOptions();
        $options['selector'] = $this->Model->selector;
        $response = $this->Model->getList($where, $options);

        $this->set_response($response);
    }
    
    public function index_put($id = null)
    {
        $data = $this->input->post_stream();
        $this->load->model('Promos');

        $id = $this->Promos->edit($data, ['id' => $id]);
        $response = [
            'result' => $id > 0
        ];
        $this->set_response($id);
    }
}
