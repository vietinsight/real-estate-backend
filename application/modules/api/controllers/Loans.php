<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/API_Controller.php';


class Loans extends API_Controller {

    protected $model = 'Loans';
    protected $selector;


    function __construct()
    {
        parent::__construct();

        $this->selector = $this->Model->selector;
    }


    public function calculator_post()
    {
        $inputs = $this->input->post_stream();
        $payment = $this->Model->calculator($inputs);

        $response = [
            'result' => $payment > 0,
            'data' => $payment,
            'message' => '(rate_per_period * present_value * margin_of_finance/100) / (1 - (1 + rate_per_period)^-number_of_periods)'
        ];
        $this->set_response($response);
    }
}
