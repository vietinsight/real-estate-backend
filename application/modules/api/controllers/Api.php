<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/API_Controller.php';


class Api extends CI_Controller {


    function index() {
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');

        echo json_encode([
            'result' => true,
            'code' => 200,
            'message' => 'Welcome to SmartProperty API'
        ]);
    }
}
