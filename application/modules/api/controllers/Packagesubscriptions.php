<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/API_Controller.php';


class Packagesubscriptions extends API_Controller {

    protected $model = 'PackageSubscriptions';
    protected $selector;


    function __construct()
    {
        parent::__construct();

        $this->selector = $this->Model->selector;
    }

    public function index_post()
    {
        $data = $this->input->post_stream();

        $res = $this->Model->create($data);
        if (is_int($res) && $res > 0) {
            $response = [
                'result' => true,
                'data' => $this->Model->getDetail($res, $this->selector)
            ];
        } else {
            $response = [
                'result' => false,
                'status' => 400,
                'error' => $res,
                'message' => 'Bad Request: validation errors, missing data',
                'data' => $data
            ];
        }
        return $this->set_response($response);
    }
}
