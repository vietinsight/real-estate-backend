<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/API_Controller.php';

use Elasticsearch\ClientBuilder;


class Search extends API_Controller {


	public function index_get($s = null)
	{
		$this->_search('listing');
	}


	public function agent_get()
	{
		if (isset($_GET['keyword'])) {
			$this->load->database();
			$keyword = str_replace('"', '', $this->input->get('keyword'));
			$speciality = $this->input->get('speciality');
			if (preg_match('#^(\w+)$#', $speciality) == FALSE) {
				$speciality = NULL;
			}

			$this->load->model('Users');
			$whereOr = [];
			$whereAnd = [];
			$fields = ['email', 'phone', 'name', 'cea_name'];
			foreach ($fields as $field) {
				$whereOr[] = '`' . $field . '` LIKE "%' . $keyword . '%"';
			}
			$whereAnd[] = '(' . implode(' OR ', $whereOr) . ')';

			if (!empty($speciality)) {
				$whereAnd[] = '`specializations` LIKE "%' . $speciality . '%"';
			}

			$wheres = [
				['(' . implode(' AND ', $whereAnd) . ')', null, false]
			];
			$options = $this->getOptions();
			$res = $this->Users->getList($wheres, $options);

			$this->set_response($res);
		} elseif (isset($_GET['q'])) {
			$this->_search('agent');
		}
	}


	public function district_get()
	{
		$this->_search('district');
	}

	public function project_get()
	{
		$this->_search('project');
	}

	public function hdb_get()
	{
		$this->_search('hdb');
	}


	public function suggest_get()
	{
		$query = trim($this->input->get('q', true));
		if ($query === '') {
			$result = [
				'result' => false,
				'reason' => 'keyword_not_provided',
				'message' => 'Keyword parameter (q) is not provided'
			];
		} elseif (strlen($query) < 1) {
			$result = [
				'result' => false,
				'reason' => 'keyword_too_short',
				'message' => 'Search terms must be at least 1 characters in length'
			];
		} else {
			$queryGET = [];

			$page = (int) $this->input->get('page', true);
			$limit = (int) $this->input->get('limit', true);

			if ($limit > 1 && $limit <= 50) {
				$queryGET['size'] = $limit;
			} else {
				$limit = 10;
			}

			if ($page > 1) {
				$queryGET['from'] = ($page-1)*$limit;
			} else {
				$page = 1;
			}

			$queryGET['filter_path'] = 'hits.total,hits.max_score,hits.hits._source';
			$queryGET['q'] = '*' . $query . '*';

			$types = [
				'district',
				'project',
				'hdb'
			];
			$queryString = http_build_query($queryGET);
			$data = [];

			foreach ($types as $type) {
				$res = eSearch($type . '/_search?' . $queryString);
				if (isset($res->hits) && $res->hits->total > 0) {
					$data[$type] = [
						'total' => $res->hits->total,
						'paging' => [
							'page' => $page,
							'limit' => $limit,
							'pages' => ceil($res->hits->total/$limit)
						],
						'max_score' => $res->hits->max_score,
						'data' => array_map(function($r){return $r->_source;}, $res->hits->hits)
					];
				}
			}

			if (empty($data)) {
				$result = [
					'result' => false,
					'reason' => 'no_results',
					'message' => 'No Results',
					'data' => []
				];
			} else {
				$result = [
					'result' => true,
					'data' => $data
				];
			}
		}

		echo json_encode($result);
	}

	public function index_post()
	{
		$this->load->model('Listings');
		$data = $this->input->post_stream();

		$client = Elasticsearch\ClientBuilder::create()->build();

		$config = $this->config->load('elastic_search', true);

		$esParams = [
		    'index' => $config['index'],
		    'type' => $this->Listings->table
		];

		$params = $this->_getParams();
		$inputs = $params['inputs'];
		unset($params['inputs']);
		$esParams = array_merge($esParams, $params);

		// Filter
		if (!empty($data['filters'])) {
			$bools = [];
			foreach ($data['filters'] as $key => $value) {
				if (is_array($value)) {
					if (count($value) > 0) {
						$cp = ['gte' => $value[0]];
						if (isset($value[1])) {
							$cp['lte'] = $value[1];
						}
						$bools['filter']['range'][$key] = [$cp];
					}
				} else {
					if ($key === 'with_photo') {
						$key = 'withPhoto';
						$value = (bool) $value;
					}

					$bools['must'][] = ['match' => [$key => $value]];
				}
			}

			if (!empty($bools)) {
				$esParams['body']['query']['bool'] = $bools;
			}
		}

		// Sorting
		if (!empty($data['sorts'])) {
			foreach ($data['sorts'] as $key => $value) {
				$esParams['body']['sort'][] = [$key => strtolower($value)];
			}
		}


		$res = $client->search($esParams);
		$total = $res['hits']['total'];
		$maxScore = $res['hits']['max_score'];

		if ($total > 0) {
			$result = [
				'result' => true,
				'total' => $total,
				'paging' => [
					'page' => $inputs['page'],
					'limit' => $inputs['limit'],
					'pages' => ceil($total/$inputs['limit'])
				],
				'max_score' => $maxScore,
				'data' => array_map(function($r){return $r['_source'];}, $res['hits']['hits'])
			];
		} else {
			$result = [
				'result' => false,
				'reason' => 'no_results',
				'message' => 'No Results',
				'data' => []
			];
		}

		echo json_encode($result);
	}


	private function _search($collection, $return = FALSE)
	{
		$query = trim($this->input->get('q', true));

		if ($query === '') {
			$result = [
				'result' => false,
				'reason' => 'keyword_not_provided',
				'message' => 'Keyword parameter (q) is not provided'
			];
		} elseif (strlen($query) < 1) {
			$result = [
				'result' => false,
				'reason' => 'keyword_too_short',
				'message' => 'Search terms must be at least 1 characters in length'
			];
		} else {
			$queryGET = [];

			$data = $this->input->get();

			$params = $this->_getParams();
			$inputs = $params['inputs'];
			unset($params['inputs']);

			// Filter
			if (!empty($data['filters'])) {
				$filters = json_decode($data['filters'], true);
				if ($filters) {
					$bools = [];
					foreach ($filters as $key => $value) {
						if (is_array($value)) {
							if (count($value) > 0) {
								$cp = ['gte' => $value[0]];
								if (isset($value[1])) {
									$cp['lte'] = $value[1];
								}
								$bools['filter']['range'][$key] = [$cp];
							}
						} else {
							$bools['must'][] = ['match' => [$key => $value]];
						}
					}

					if (!empty($bools)) {
						$esParams['body']['query']['bool'] = $bools;
					}
				}
			}

			// if (!empty($data['filters'])) {
			// 	$filters = json_decode($data['filters'], true);
			// 	if ($filters) {
			// 		foreach ($filters as $key => $value) {
			// 			$params['']
			// 		}
			// 	}
			// }

			// Sorting
			if (!empty($data['sorts'])) {
				$sorts = json_decode($data['sorts'], true);
				if ($sorts) {
					foreach ($sorts as $key => $value) {
						$params['sort'] = $key . ':' . strtolower($value);
					}
				}
			}

			$params['q'] = '*' . $query . '*';

			$res = eSearch($collection . '/_search?' . http_build_query($params));

			$total = isset($res->hits->total) ? $res->hits->total : 0;
			$maxScore = isset($res->hits->max_score) ? $res->hits->max_score : 0;

			$page = $inputs['page'];
			$limit = $inputs['limit'];
			$offset = ($page - 1) * $limit;

			//array_map(function($r){return $r->_source;}, $res->hits->hits)

			if ($total > 0) {

				$data = array_map(function($r){return $r->_source;}, $res->hits->hits);
				$this->load->model('Listings');
				$data = $this->Listings->getNested($data);

				$result = [
					'result' => true,
					'total' => $total,
					'paging' => [
						'total' => $total,
						'page' => $page,
						'limit' => $limit,
						'offset' => $offset,
						'pages' => ceil($total/$limit)
					],
					'max_score' => $maxScore,
					'data' => $data
				];
			} else {
				$result = [
					'result' => false,
					'reason' => 'no_results',
					'message' => 'No Results',
					'data' => []
				];
			}
		}

		if ($return) {
			return $result;
		} else {
			echo json_encode($result);
		}
	}


	private function _getParams()
	{
		$params = [];

		$page = (int) $this->input->get('page', true);
		$limit = (int) $this->input->get('limit', true);

		if ($limit > 0 && $limit <= 50) {
			$params['size'] = $limit;
		} else {
			$limit = 10;
		}

		if ($page > 1) {
			$params['from'] = ($page-1)*$limit;
		} else {
			$page = 1;
		}

		$params['filter_path'] = 'hits.total,hits.max_score,hits.hits._source';
		$params['inputs'] = [
			'page' => $page,
			'limit' => $limit
		];

		return $params;
	}
}
