<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/API_Controller.php';


class Posts extends API_Controller {

    protected $model = 'Posts';
    protected $selector;


    function __construct()
    {
        parent::__construct();

        $this->selector = $this->Model->selector;
    }
}
