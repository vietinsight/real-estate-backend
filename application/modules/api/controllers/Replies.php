<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/API_Controller.php';


class Replies extends API_Controller {

    protected $model = 'Replies';
    protected $selector = 'comment.uuid as id, user.uuid as user_id, comment.listing_id, user.email, user.phone, user.avatar, comment.message, comment.time_created';


    public function replies_post($parentId)
    {
        $res = $this->Model->getDetailCache('uuid', $parentId);
        if (is_null($res)) {
            $response = ['result' => false];
        } else {
            $this->load->model('Comments');
            $data = $this->input->post_stream();
            $userId = $this->auth->userId();
            if ($userId) {
                $data['user_id'] = $userId;
            }
            $data['parent_id'] = $res->id;
            $data['listing_id'] = $res->listing_id;
            $id = $this->Comments->create($data);

            if ($id) {
                $response = [
                        'result' => true,
                        'data' => $this->Comments->getDetail($id, 'uuid as id, message, time_created')
                    ];
            }
        }

        $this->set_response($response);
    }


    public function replies_put($parentId)
    {

    }


    public function replies_delete($parentId)
    {

    }
}
