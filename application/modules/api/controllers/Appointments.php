<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/API_Controller.php';


class Appointments extends API_Controller {

    protected $model = 'Appointments';


    public function index_post()
    {
        $this->auth->checkToken();

        $data = $this->input->post_stream();
        $error = null;
        
        if (!empty($data['partner_id'])) {
            $this->load->model('Users');
            $field = is_numeric($data['partner_id']) ? 'id' : 'uuid';
            $partner = $this->Users->getDetailCache($field, $data['partner_id'], 'id');
            if ($partner) {
                $data['partner_id'] = $partner->id;

                if (!empty($data['listing_id'])) {
                    $this->load->model('Listings');
                    $field = is_numeric($data['listing_id']) ? 'id' : 'uuid';
                    $listing = $this->Listings->getDetailCache($field, $data['listing_id'], 'id');
                    if ($listing) {
                        $data['listing_id'] = $listing->id;
                    } else {
                        unset($data['listing_id']);
                    }
                }

                $data['user_id'] = $this->auth->userId();

                $res = $this->Model->create($data);

                if (!is_numeric($res)) {
                    $error = $res;
                }
            } else {
                $error = [];
            }
        } else {
            $error = [];
        }

        if (is_null($error)) {
            $response = [
                'result' => true,
                'id' => $res,
                'data' => [
                    'id' => $res
                ]
            ];
        } else {
            $response = [
                'result' => false,
                'status' => 400,
                'reason' => 'bad_request',
                'message' => 'Bad Request: validation errors, missing or exist data'
            ];
            if (!empty($error)) {
                $response['errors'] = $error;
            }
        }
        $this->set_response($response);
    }


    public function index_get($id = null)
    {
        $this->auth->checkToken();

        if (!is_null($id)) {
            parent::index_get($id);
        } else {
            $options = $this->getOptions();
            $wheres = ['partner_id' => $this->auth->userId()];
            if (!empty($_GET['date']) && preg_match('#^\d{4}-\d{1,2}-\d{1,2}$#', $_GET['date'])) {
                $wheres['DATE(`booking_time`)'] = $_GET['date'];
            }
            $response = $this->Model->getList($wheres, $options);
            $this->set_response($response);
        }
    }


    public function me_get()
    {
        $this->auth->checkToken();

        $options = $this->getOptions();
        $wheres = ['user_id' => $this->auth->userId()];
        if (!empty($_GET['date']) && preg_match('#^\d{4}-\d{1,2}-\d{1,2}$#', $_GET['date'])) {
            $wheres['DATE(`booking_time`)'] = $_GET['date'];
        }
        $response = $this->Model->getList($wheres, $options);
        $this->set_response($response);
    }


    public function all_get()
    {
        $this->auth->checkToken();

        $options = $this->getOptions();
        $wheres['(user_id = "' . $this->auth->userId() . '" OR partner_id = "' . $this->auth->userId() . '")'] = null;
        if (!empty($_GET['date']) && preg_match('#^\d{4}-\d{1,2}-\d{1,2}$#', $_GET['date'])) {
            $wheres['DATE(`booking_time`)'] = $_GET['date'];
        }
        $response = $this->Model->getList($wheres, $options);
        $this->set_response($response);
    }


    public function index_put($id = null)
    {
        $this->auth->checkToken();

        $data = $this->input->post_stream();

        $this->load->model('Emails');
        // $this->Emails->set_time($data['email']);
        // $value['partner_id'] = $data['partner_id'];
        $id = $this->Model->edit($data, ['id' => $id]);

        $response = [
            'result' => $id > 0,
            'code' => 200
        ];

        $this->set_response($response);
    }


    public function timelines_post($type = 'free', $listingId = null)
    {
        $this->auth->checkToken();
        
        $data = $this->input->post_stream();
        $res  = $this->Model->setFreeTime($data, $listingId);

        if ($res) {
            $response = [
                'result' => true,
                'code' => 201
            ];
        } else {
            $response = [
                'result' => false,
                'code' => 400,
                'reason' => 'bad_request'
            ];
        }

        $this->set_response($response);
    }


    public function timelines_get($userId = null)
    {
        if (is_null($userId)) {
            $userId = $this->auth->userId();
        }
        $this->load->model('Users');
        $field = is_numeric($userId) ? 'id' : 'uuid';
        $user = $this->Users->getDetailCache($field, $userId, 'uuid as id, id as __id, name, email, phone');
        if (is_null($user)) {
            $response = [
                'result' => false,
                'code' => 400,
                'reason' => 'not_found',
                'message' => 'User is not exist'
            ];
        } else {
            $freeTimes = [];
            for ($i = 0; $i < 7; $i++) {
                $times = [];
                $j = 8;
                while ($j <= 20) {
                    $from = $j + mt_rand(0, 3);
                    $to = $from + mt_rand(1, 3);
                    if ($to <= 20) {
                        $times[] = [
                            'from' => $from,
                            'to' => $to
                        ];
                    }
                    $j = $to + 1;
                }
                $freeTimes[] = [
                    'date' => date('Y-m-d', strtotime('+' . $i . ' day')),
                    'times' => $times
                ];
            }
            $response = [
                'result' => true,
                'data' => [
                    'user' => $user,
                    'free_times' => $freeTimes
                ]
            ];
        }


        $this->set_response($response);
    }
}
