<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/API_Controller.php';


class Feedbacks extends API_Controller {

    protected $model = 'Feedbacks';


    public function index_post()
    {
        $data = $this->input->post_stream();
        if (!empty($data['receiver_id'])) {
            $this->load->model('Users');
            $field = is_numeric($data['receiver_id']) ? 'id' : 'uuid';
            $user = $this->Users->getDetailByField($field, $data['receiver_id'], 'id');
            $data['receiver_id'] = $user->id;
        }

        $this->setData($data);

        parent::index_post();
    }


    public function index_get($id = null)
    {
        $this->auth->checkToken();

        $userId = $this->auth->userId();
        if (is_null($userId)) {
            $this->set_response([
                    'result' => false,
                    'reason' => 'guest',
                    'message' => 'Please login to show your feedbacks'
                ]);
        } else {
            $this->setCondition(['user_id' => $userId]);

            parent::index_get($id);
        }
    }


    public function me_get()
    {
        $this->auth->checkToken();

        $userId = $this->auth->userId();
        $this->setCondition(['receiver_id' => $userId]);

        parent::index_get($id);
    }
}
