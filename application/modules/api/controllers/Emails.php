<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/API_Controller.php';


class Emails extends API_Controller {

    protected $model = null;
    protected $selector = null;

    function __construct()
    {
    	parent::__construct();

    	$this->load->model('Emails');
    }


    public function index_get($id = null) {}
    public function index_post() {}
    public function index_put($id = null) {}
    public function index_patch($id = null) {}
    public function index_delete($id = null) {}


    public function user_register_get()
    {
    	$this->Emails->user_register();
    }


    public function user_forget_password_post()
    {

    }


    public function test_post()
    {
        $email = $this->input->post_stream('email');
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->load->library('Mailer');
            $res = $this->mailer
                    ->setTitle('[Test] Title ' . time())
                    ->setBody('Content ' . date('Y-m-d H:i:s'))
                    ->add($email, 'Name')
                    ->send();
            $response = [
                'result' => $res
            ];
        } else {
            $response = [
                'result' => false,
                'message' => 'Email is invalid'
            ];
        }

        $this->set_response($response);
    }
}
