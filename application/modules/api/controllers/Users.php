<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/API_Controller.php';


class Users extends API_Controller
{

    protected $model = 'Users';
    protected $selector;


    function __construct()
    {
        parent::__construct();

        $this->selector = $this->Model->selector;
    }


    public function authentication_post($result = NULL)
    {
        if (is_null($result)) {
            $result = $this->Model->login();
        }

        $response = [
            'result' => !empty($result)
        ];

        if ($response['result']) {
            $fields = [
                'id',
                'uuid',
                'name',
                'email',
                'phone',
                'gender',
                'type',
                'avatar'
            ];
            $user   = new stdClass();
            foreach ($result as $key => $value) {
                if (in_array($key, $fields)) {
                    $user->$key = $value;
                }
            }
            $user->__id = $user->id;
            $user->id   = $user->uuid;
            unset($user->uuid);
            if ($result->status == 1 OR $result->status === 'Active') {
                $response = [
                    'result' => TRUE,
                    'status' => 200,
                    'data' => $this->Model->getNested([$user])[0],
                    'token' => $this->auth->generateToken($result)
                ];
            }
            else {
                $response = [
                    'result' => FALSE,
                    'status' => 403,
                    'message' => 'This account is not confirmed'
                ];
            }
        }
        else {
            $response = [
                'result' => FALSE,
                'status' => 400,
                'message' => 'Email and password do not match'
            ];
        }

        $this->set_response($response);
    }


    public function authentication_renew_post()
    {
        $oldToken = $this->input->post_stream('token');
        $payLoad  = $this->auth->getPayload(NULL, $oldToken);
        if (!empty($payLoad->id)) {
            $this->load->model('Users');
            $user = $this->Users->getDetailCache('id', $payLoad->id);
            if (!is_null($user) && $payLoad->at === $user->access_token) {
                return $this->authentication_post($user);
            }
        }

        $this->set_response([
            'result' => FALSE,
            'status' => 400
        ]);
    }


    public function authentication_delete()
    {
        $userId = $this->auth->userId();

        if ($userId) {
            $this->db->set('access_token', 'UUID()', FALSE);
            $this->Model->update([], ['id' => $userId]);
        }

        $response = [
            'result' => TRUE
        ];

        $this->set_response($response);
    }


    public function authentication_3party_post($channel = NULL)
    {
        $channel  = strtolower($channel);
        $channels = ['facebook'];

        $accessToken = $this->input->post_stream('access_token');

        if (in_array($channel, $channels)) {
            if ($channel === 'facebook') {
                $config   = include(APPPATH . '/config/hybrid_auth.php');
                $fbConfig = $config['providers']['Facebook']['keys'];
                $fb       = new Facebook\Facebook([
                    'app_id' => $fbConfig['id'],
                    'app_secret' => $fbConfig['secret'],
                    'default_graph_version' => 'v2.7'
                ]);

                $data = [];

                if (!empty($accessToken)) {
                    try {
                        $response = $fb->get('/me?fields=id,name,email', $accessToken);
                        $me       = $response->getGraphUser();
                        $data     = $me->asArray();
                    }
                    catch (Facebook\Exceptions\FacebookResponseException $e) {
                        echo 'Graph returned an error: ' . $e->getMessage();
                        exit;
                    }
                    catch (Facebook\Exceptions\FacebookSDKException $e) {
                        echo 'Facebook SDK returned an error: ' . $e->getMessage();
                        exit;
                    }
                }

                $response = [];


                if (!empty($data)) {
                    if (isset($data['email'])) {
                        $this->load->model('Users');
                        $params           = [];
                        $user             = $this->Users->getDetailCache('email', $data['email']);
                        $params['status'] = is_null($user) ? 'New' : 'Exist';
                        if (is_null($user)) {
                            $userData = [
                                'email' => $data['email'],
                                'password' => md5(base64_encode(mcrypt_create_iv(32))),
                                'social' => 'facebook',
                                'name' => empty($data['name']) ? 'Facebooker_' . $data['id'] : $data['name'],
                                'type' => 'Buyer',
                                'status' => 1
                            ];
                            $userId   = $this->Users->create($userData);
                            $user     = $this->Users->getDetailCache('id', $userId);
                        }

                        return $this->authentication_post($user);
                    }
                    else {
                        $response = [
                            'result' => FALSE,
                            'status' => 400,
                            'reason' => 'no_email',
                            'message' => 'No email data'
                        ];
                    }
                }
                else {
                    $response = [
                        'result' => FALSE,
                        'status' => 400,
                        'reason' => 'no_data',
                        'message' => 'No data'
                    ];
                }

                $this->set_response($response);
            }
        }
    }


    public function drop_post()
    {
        $wheres = [];
        foreach ([
                     'uuid',
                     'email'
                 ] as $key) {
            $value = $this->input->post_stream($key);
            if (!is_null($value)) {
                $wheres[$key] = $value;
                break;
            }
        }

        $user = $this->Model->getDetailCache($key, $value);
        if (!is_null($user)) {
            $this->Model->delete(['id' => $user->id]);
            $tables = [
                'appointment',
                'comment',
                'comment',
                'like',
                'listing'
            ];
            foreach ($tables as $table) {
                $this->db->where('user_id', $user->id)->delete($table);
            }
            $this->db->where('user_id', $user->id)->or_where('receiver_id', $user->id)->delete('message');
        }

        $this->set_response(['result' => !is_null($user)]);
    }


    public function check_token_post()
    {
        $token = $this->input->post_stream('token');
        if (empty($token)) {
            $token = $this->auth->getToken();
        }
        $data    = $this->auth->getPayload(NULL, $token);
        $timeout = $this->auth->getTimeout();

        if (empty($data)) {
            $response = [
                'result' => FALSE,
                'reason' => 'token_invalid'
            ];
        }
        else {
            $remain = $data->iat + $timeout - time();
            if ($remain > 0) {
                $response = [
                    'result' => TRUE,
                    'issue_at' => $data->iat,
                    'issue_datetime' => date('Y-m-d H:i:s', $data->iat),
                    'remain' => $data->iat + $timeout - time()
                ];
            }
            else {
                $response = [
                    'result' => FALSE,
                    'reason' => 'token_expired',
                    'issue_at' => $data->iat,
                    'issue_datetime' => date('Y-m-d H:i:s', $data->iat),
                ];
            }
        }

        $this->set_response($response);
    }


    public function index_get($userId = null)
    {
        $this->profile_get($userId);
    }


    public function profile_get($userId = NULL)
    {
        if (is_null($userId)) {
            $this->auth->checkToken();

            $userId = $this->auth->userId();
            $field  = 'id';
        }
        else {
            $field = 'uuid';
        }
        $user = $this->Model->getDetailCache($field, $userId, $this->selector);

        if (!empty($user->regions_covered)) {
            $regionsCovered = $this->getLocationSearch('district', ['id' => $user->regions_covered]);
            $user->values['regions_covered'] = $regionsCovered;
        }

        if (!empty($user->hdb_estates_covered)) {
            $hdbEstatesCovered = $this->getLocationSearch('hdb', ['id' => $user->hdb_estates_covered]);
            $user->values['hdb_estates_covered'] = $hdbEstatesCovered;
        }

        $response = [
            'result' => !is_null($user),
            'data' => $user
        ];
        $this->set_response($response);
    }


    private function getLocationSearch($type, $where)
    {
        $client = Elasticsearch\ClientBuilder::create()->build();
        $config = $this->config->load('elastic_search', true);

        $esParams = [
            'index' => $config['index'],
            'type' => $type,
            'body' => [
                'query' => [
                    'constant_score' => [
                        'filter' => [
                            'terms' => $where
                        ]
                    ]
                ]
            ]
        ];
        $res = $client->search($esParams);

        return array_map(function ($o){
            return $o['_source'];
        }, $res['hits']['hits']);
    }


    public function profile_put()
    {
        $this->auth->checkToken();

        $userId = $this->auth->userId();
        $data   = $this->input->post_stream();
        $result = $this->Model->edit($data, ['id' => $userId]);

        $response = [
            'result' => $result === TRUE
        ];

        if ($result === TRUE) {
            $result = $this->Model->getDetailByField('id', $userId, $this->selector);
            if ($result) {
                $response['data'] = $result;
            }
            else {
                $response['result'] = FALSE;
                $response['status'] = 400;
            }
        }
        else {
            $response['status']  = 400;
            $response['errors']  = $result;
            $response['message'] = 'Bad Request: validation errors, missing or exist data';
        }

        $this->set_response($response);
    }


    public function activation_post()
    {
        $email = $this->input->post_stream('email');
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $user = $this->Model->getDetailCache('email', $email);
            if (!is_null($user)) {
                if ($user->status === 'InActive') {
                    $this->load->model('Emails');
                    $this->Emails->user_register($user);
                    $response = [
                        'result' => TRUE,
                        'status' => 201,
                        'message' => 'Email activation is sent to ' . $email
                    ];
                }
                else {
                    $response = [
                        'result' => TRUE,
                        'status' => 200,
                        'message' => 'This account is ready'
                    ];
                }
            }
            else {
                $response = [
                    'result' => FALSE,
                    'status' => 400,
                    'message' => 'This email is not exists in system'
                ];
            }
        }
        else {
            $response = [
                'result' => FALSE,
                'status' => 400,
                'message' => 'The email is not valid'
            ];
        }

        $this->set_response($response);
    }


    public function forget_password_post()
    {
        $email = $this->input->post_stream('email');
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $user = $this->Model->getDetailRawCache('email', $email);
            if ($user) {
                $this->load->model('Emails');
                $this->Emails->forgetPassword($user);

                $response = [
                    'result' => true,
                    'code' => 200,
                    'message' => 'Please check your email, and reset your password'
                ];
            } else {
                $response = [
                    'result' => false,
                    'code' => 404,
                    'reason' => 'not_found',
                    'message' => 'This email is not exist in our system'
                ];
            }
        } else {
            $response = [
                'result' => false,
                'code' => 400,
                'message' => 'Email is invalid'
            ];
        }

        $this->set_response($response);
    }


    public function wishlists_post()
    {
        $this->auth->checkToken();

        $data = $this->input->post_stream();
        $this->Model->setWishlist($data);

        $response = [
            'result' => TRUE,
            'code' => 201
        ];

        $this->set_response($response);
    }


    public function wishlists_get()
    {
        $this->auth->checkToken();

        $this->load->model('Listings');
        $userId   = $this->auth->userId();
        $wishlist = $this->Model->getWishlist($userId);
        if (is_null($wishlist)) {
            $response = [
                'result' => FALSE,
                'code' => 204,
                'reason' => 'no_data',
                'message' => 'No data'
            ];
        }
        else {
            $response = [
                'result' => TRUE,
                'code' => 200,
                'data' => $wishlist
            ];
        }

        $this->set_response($response);
    }


    public function wishlists_delete()
    {
        $this->auth->checkToken();

        $this->Model->setWishlist(NULL);

        $response = [
            'result' => TRUE,
            'code' => 200
        ];

        $this->set_response($response);
    }


    public function shortlists_get()
    {
        $this->auth->checkToken();

        $userId = $this->auth->userId();

        $this->load->model('Listings');

        $wheres = [
            'like.user_id' => $userId,
            'like.shortlist' => 1
        ];
        if (!empty($_GET['folder_id']) && is_numeric($_GET['folder_id'])) {
            $wheres['like.shortlist_folder'] = (int) $_GET['folder_id'];
        }
        $options             = $this->getOptions();
        $options['selector'] = $this->Listings->selector . ', like.user_id';
        $options['joins']    = [
            [
                'like',
                'like.listing_id = listing.id'
            ]
        ];

        $response = $this->Listings->getList($wheres, $options);
        $this->set_response($response);
    }


    public function shortlists_delete($listingId)
    {
        $this->auth->checkToken();

        $userId = $this->auth->userId();
        $type   = 'shortlist';
        $this->load->model('Likes');
        $Type  = $this->Likes->types[$type];
        $data  = [
            $Type => 0,
            'time_' . $Type => NULL
        ];
        $where = [
            'user_id' => $userId,
            'listing_id' => $listingId
        ];

        $this->Likes->update($data, $where);

        $response = [
            'result' => TRUE
        ];
        $this->set_response($response);
    }


    public function shortlist_folders_post()
    {
        $this->auth->checkToken();

        $this->load->model('ShortlistFolders');
        $data            = $this->input->post_stream();
        $data['user_id'] = $this->auth->userId();
        $res             = $this->ShortlistFolders->create($data);

        if (is_numeric($res)) {
            $response = [
                'result' => TRUE,
                'code' => 201,
                'data' => [
                    'id' => $res
                ]
            ];
        }
        else {
            $response = [
                'result' => FALSE,
                'code' => 409,
                'reason' => 'conflict',
                'message' => 'Folder name is exist'
            ];
        }

        $this->set_response($response);
    }


    public function shortlist_folders_get()
    {
        $this->auth->checkToken();

        $this->load->model('ShortlistFolders');
        $wheres              = [
            'user_id' => $this->auth->userId()
        ];
        $options             = $this->getOptions();
        $options['selector'] = $this->ShortlistFolders->selector;
        $response            = $this->ShortlistFolders->getList($wheres, $options);

        $this->set_response($response);
    }


    public function shortlist_folders_put($id = NULL)
    {
        $this->auth->checkToken();

        $this->load->model('ShortlistFolders');
        $folderName = $this->input->post_stream('name');
        $data       = [
            'name' => $folderName
        ];
        $wheres     = [
            'user_id' => $this->auth->userId(),
            'id' => $id
        ];
        $this->ShortlistFolders->update($data, $wheres);

        $response = [
            'result' => TRUE,
            'code' => 200
        ];
        $this->set_response($response);
    }


    public function shortlist_folders_delete($id = NULL)
    {
        $this->auth->checkToken();
        $this->load->model('ShortlistFolders');
        $this->ShortlistFolders->delete($id);

        $response = [
            'result' => TRUE,
            'code' => 200
        ];
        $this->set_response($response);
    }


    public function count_get($type = NULL)
    {
        if ($type === 'shortlist') {
            $this->load->model('Listings');

            $response = [
                'result' => TRUE,
                'data' => $this->Listings->shortlistTotal()
            ];

            $this->set_response($response);
        }
    }


    public function listings_get($userId = NULL)
    {
        if (is_null($userId)) {
            $this->auth->checkToken();
            $userId = $this->auth->userId();
        } else {
            $field = is_numeric($userId) ? 'id' : 'uuid';
            $this->load->model('Users');
            $user = $this->Users->getDetailRawCache($field, $userId, 'id');
            if ($user) {
                $userId = $user->id;
            }
        }

        $this->load->model('Listings');

        $wheres              = ['user_id' => $userId];
        $options             = $this->getOptions();
        $options['selector'] = $this->Listings->selector;

        $response = $this->Listings->getList($wheres, $options);
        $this->set_response($response);
    }


    public function alerts_get()
    {
        $this->auth->checkToken();

        $this->load->model('Listings');
        $this->load->model('Users');

        $userId   = $this->auth->userId();
        $user     = $this->Users->getDetailCache('id', $userId);
        $userRole = $user->type;
        if ($userRole === 'Buyer') {
            $wishlist = $this->Users->getWishlist(NULL, $user);
            if (is_null($wishlist)) {
                $response = [
                    'result' => FALSE,
                    'code' => 204,
                    'reason' => 'no_data',
                    'message' => 'No data'
                ];
            }
            else {
                foreach ($wishlist as $key => $value) {
                    if (is_array($value) && count($value) >= 2 && is_numeric($value[0]) && is_numeric($value[1])) {
                        $wheres[$key . ' >='] = $value[0];
                        $wheres[$key . ' <='] = $value[1];
                    }
                    elseif (is_scalar($value)) {
                        if ($key === 'price') {
                            $wheres[$key . ' >='] = $value * 0.8;
                            $wheres[$key . ' <='] = $value * 1.2;
                        }
                        else {
                            $wheres[$key] = $value;
                        }
                    }
                }

                $options             = $this->getOptions();
                $options['selector'] = $this->Listings->selector;
                $response            = $this->Listings->getList($wheres, $options);
            }
        }
        else {
            $response = [
                'result' => FALSE,
                'code' => 204,
                'reason' => 'no_data',
                'message' => 'No data'
            ];
        }

        $this->set_response($response);
    }


    public function activities_get()
    {
        $this->auth->checkToken();

        $this->load->model('Report');

        $data = [
            'listing' => $this->Report->listing(),
            'message' => $this->Report->message(),
            'activity' => $this->Report->activity(),
            'subcribe' => [
                'follower' => 25,
                'following' => 12
            ],
            'appointment' => $this->Report->appointment()
        ];

        $response = [
            'result' => TRUE,
            'data' => $data
        ];

        $this->set_response($response);
    }


    public function messages_get()
    {
        $this->auth->checkToken();

        $this->load->model('Messages');
        $userId = $this->auth->userId();

        $options = $this->getOptions();
        unset($options['selector']);
        $wheres['(user_id = "' . $userId . '" OR receiver_id = "' . $userId . '")'] = NULL;

        $options['group_by'] = ['channel'];

        $response = $this->Messages->getList($wheres, $options);

        $this->set_response($response);
    }


    public function comments_get()
    {
        $this->auth->checkToken();

        $this->load->model('Comments');
        $userId = $this->auth->userId();

        $options             = $this->getOptions();
        $options['selector'] = $this->Comments->selector;
        $response            = $this->Comments->getList(['user_id' => $userId], $options);

        $this->set_response($response);
    }


    // TODO
    public function tickets_get()
    {
        $response = [
            'result' => FALSE,
            'code' => 204,
            'reason' => 'no_data',
            'message' => 'No data'

        ];
        $this->set_response($response);
    }


    // TODO
    public function reviews_get()
    {
        $response = [
            'result' => FALSE,
            'code' => 204,
            'reason' => 'no_data',
            'message' => 'No data'

        ];
        $this->set_response($response);
    }


    // TODO
    public function posts_get()
    {
        $response = [
            'result' => FALSE,
            'code' => 204,
            'reason' => 'no_data',
            'message' => 'No data'

        ];
        $this->set_response($response);
    }


    public function specializations_get()
    {
        $response = $this->Model->listSpecializations([], isset($_GET['raw']));
        $this->set_response($response);
    }


    public function hot_agents_get()
    {
        $types = array_unique(array_filter(explode(',', $this->input->get('types'))));
        $limit = (int) $this->input->get('limit');
        if ($limit < 1 OR $limit > 10) {
            $limit = 4;
        }

        $list = [
            'commercial' => 'Commercial Agents',
            'hdb' => 'HDB Sales/Rental',
            'industrial' => 'Industrial Featured Agents',
            'landed' => 'Landed Featured Agents',
            'luxury' => 'Luxury Property Featured Agents',
            'condo' => 'Private Condo Sales/Rental'
        ];

        foreach ($types as $k => $type) {
            if (!isset($list[$type])) {
                unset($types[$k]);
            }
        }

        if (empty($types)) {
            $types = array_keys($list);
        }

        $data = [];

        foreach ($types as $type) {
            $agents = $this->Model->getList([
                ['`specializations` REGEXP "' . $type . '"', null, false]
            ], [
                'limit' => $limit,
                'selector' => 'id as __id, uuid as id, name, email, phone, title, address, avatar'
            ]);

            if ($agents['paging']['total'] > 0) {
                $data[$type] = [
                    'title' => $list[$type],
                    'data' => $agents['records']
                ];
            }
        }

        $this->set_response([
            'result' => true,
            'code' => 200,
            'data' => $data
        ]);
    }


    public function contacts_post($partnerId = NULL)
    {
        if (!is_null($partnerId)) {
            $field = is_numeric($partnerId) ? 'id' : 'uuid';
            $this->load->model('Users');
            $partner = $this->Users->getDetailRawCache($field, $partnerId, 'id');
            if ($partner) {
                if ($this->auth->userId() != $partner->id) {
                    $data = $this->input->post_stream();
                    $data['partner_id'] = $partner->id;
                    $this->load->model('Contacts');
                    $this->Contacts->create($data);

                    $response = [
                        'result' => true,
                        'code' => 201
                    ];
                } else {
                    $response = [
                        'result' => false,
                        'code' => 406,
                        'message' => ''
                    ];
                }
            } else {
                $response = [
                    'result' => false,
                    'code' => 404,
                    'reason' => 'not_found',
                    'message' => 'Partner not found'
                ];
            }
        } else {
            $response = [
                'result' => false,
                'code' => 400
            ];
        }

        $this->set_response($response);
    }
}
