<?php

defined('BASEPATH') OR exit('No direct script access allowed');


require_once( APPPATH . "/third_party/Hybrid/Auth.php" );


class Test extends MX_Controller {


	function __construct()
	{
		parent::__construct();

        $this->load->database();
        $this->load->library('Auth');
        $this->load->model('Users');
        $this->load->model('Listings');
	}


    function redis()
    {
        $this->output->enable_profiler(TRUE);

        $this->load->driver('cache');
        $obj = new stdClass();
        $obj->name = 'Hằng';
        $obj->gender = 'Female';

        $this->cache->redis->save('foo', $obj, 10);

        // var_dump($this->cache->redis->get('foo'));


        $this->load->model('Listings');

        $listings = $this->Listings->getList();
    }


    function abc()
    {
        $string = $this->input->post('data');
        echo $string;

//        $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $string));
        $data = base64_decode(substr($string, strpos($string, ',') + 1));

        file_put_contents('anc.jpg', $data);
    }


    function msg()
    {
        $this->load->library('Xpusher');
        $this->xpusher->trigger('test_channel', 'my_event', ['result' => true, 'message' => 'hihi! now is ' . date('H:i:s')]);
    }


    function delete_listing()
    {
        $params = [
            'index' => 'sp_dev',
            'type' => 'listings'
        ];
        $client = Elasticsearch\ClientBuilder::create()->build();

        // Delete doc at /my_index/my_type/my_id
        $response = $client->delete($params);
    }


	public function index()
	{
        // header('Content-Type: application/json');

        $this->load->library('Esearch');
        $this->esearch->selectType('listings');


        $lat = -74.19851400000000;
        $lon = -119.97437500000000;

        $d = [
          "query"=> [
            "filtered" => [
                "query" => [
                    "match_all" => []
                ],
                "filter" => [
                    'bool' => [
                        'must' => [
                            "geo_distance" => [
                                "distance" => "10000km",
                                "location" => [
                                    "lat" => $lat,
                                    "lon" => $lon
                                ]
                            ]
                        ],
                        'filter' => [
                            'term' => [
                                'tenure' => 1
                            ]
                        ]
                    ]
                ]
            ]
          ],

          "sort" => [
            [
              "_geo_distance" => [
                "location" => [
                  "lat" =>  $lat,
                  "lon" => $lon
                ],
                "order" =>         "asc",
                "unit" =>          "km", 
                "distance_type" => "plane" 
              ]
            ]
          ]

        ];

        try {
            $res = $this->esearch->search($d);
            $res = array_map(function($o) use ($lat, $lon){
                $o['_source']['distance'] = distance($lat, $lon, $o['_source']['location']['lat'], $o['_source']['location']['lon'], 'k');
                return $o['_source'];
            }, $res['hits']['hits']);
        } catch (Exception $e) {
           $res = false; 
        }


        var_dump($res);

        die();


        $client = Elasticsearch\ClientBuilder::create()->build();

        // $esParams = [
        //     'index' => 'sp',
        //     'type' => 'listings',
        //     'body' => [
        //         'fields' => ['name', 'address', 'description'],
        //         "query" => [
        //             "match" => [
        //                     "name" => "nemo"
        //                 ],
        //             "match" => [
        //                     "address" => "Mission"
        //                 ]
        //             ],
        //             "highlight" => [
        //                 "fields" => [
        //                     "name" => new \stdClass(),
        //                     "address" => new \stdClass()
        //                 ]
        //             ]
        //     ]
        // ];

        // $res = $client->search($esParams);

        $params = [
            'index' => 'sp',
            'type' => 'listings',
            'body' => [
                // 'query' => [
                //     'bool' => [
                //         // 'must' => [
                //         //     'term' => [ 'unit_type' => 'Sqm' ],
                //         //     'term' => [ 'age' => 440 ]
                //         // ],
                //         'filter' => [
                //             'match' => [ 'name' => 'haru' ]
                //         ]
                //     ],
                // ]

                "suggest" => [
                  "my-suggest-1" => [
                    "text" => "haru",
                    "term" => [
                      "field" => "name"
                    ]
                  ],
                  ]
            ]
        ];

        // $res = $client->search($params);
        $res = $l->search(["suggest" => [
                  "my-suggest-1" => [
                    "text" => "haru",
                    "term" => [
                      "field" => "name"
                    ]
                  ],
                  ]]);

        echo json_encode($res);
	}


    function get_map_photo_get()
    {
        $params = [
            'maxwidth' => 400,
            'photoreference' => 'CnRtAAAATLZNl354RwP_9UKbQ_5Psy40texXePv4oAlgP4qNEkdIrkyse7rPXYGd9D_Uj1rVsQdWT4oRz4QrYAJNpFX7rzqqMlZw2h2E2y5IKMUZ7ouD_SlcHxYq1yL4KbKUv3qtWgTK0A6QbGh87GB3sscrHRIQiG2RrmU_jF4tENr9wGS_YxoUSSDrYjWmrNfeEHSGSc3FyhNLlBU',
            'key' => 'AIzaSyC7YSO1uK0JSR0mKe9LTNunY3rIlQQD2io'
        ];
        $res = curl('https://maps.googleapis.com/maps/api/place/photo', $params);
        var_dump($res);
    }


	function clean()
	{
		$this->load->database();
		$this->db->truncate('user');
		$this->db->truncate('listing');
		eSearch('', [], 'DELETE');
	}


    function fake_user($total = 10)
    {
        $tables = ['user', 'appointment', 'comment', 'draft', 'like', 'listing', 'message', 'timeline'];
        foreach ($tables as $table) {
            $this->db->truncate($table);
            removeDirectory('files/' . $table);
        }
        
        $this->buildUsers($total);
    }


	function fake($continue = false)
	{die();
		if ($continue) {
			$this->buildListings(50, false);
		} else {
            $tables = ['appointment', 'comment', 'draft', 'feedback', 'like', 'message', 'package_subscriptions', 'post', 'promos', 'subscribe', 'timeline'];
            foreach ($tables as $table) {
                $this->db->truncate($table);
            }
            
	        $this->buildUsers(10);
	        $this->buildListings(50);
		}
	}


    function buildUsers($number = 100, $truncate = true)
    {
    	if ($truncate) {
            $this->db->truncate('user');
            removeDirectory('files/user');
    	}

        $faker = \Faker\Factory::create();

        $genders = ['None', 'Male', 'Female'];
        $types = ['Admin', 'Mod', 'Agent', 'Seller', 'Buyer'];
        $countTypes = count($types);

        for ($i = 0; $i < $number; $i++) {
            if ($i < $countTypes) {
                $email = strtolower($types[$i]) . '@sp.online';
                $type = $types[$i];
            } else {
                $email = $faker->email;
                $type = $types[array_rand($types)];
            }

            $data = [
                'email' => $email,
                'password' => 123456,
                'name' => $faker->firstName,
                'phone' => trim($faker->e164PhoneNumber, '+'),
                'address' => $faker->address,
                'birthday' => $faker->date('Y-m-d', '-18 years'),
                'gender' => $genders[array_rand($genders)],
                'type' => $type,
                'access_token' => $faker->Uuid,
                'status' => 1
            ];

            // var_dump($data);
            $id = $this->Users->create($data);
            // var_dump($id);
        }
    }


    function buildListings($number = 100, $truncate = true)
    {
        $imgPath = 'files/' . $this->Listings->table;
    	if ($truncate) {
            $this->db->truncate($this->Listings->table);
            removeDirectory($imgPath);
    	}


        $userTypes = [];
        $users = $this->db->select('id, type')->where_in('type', [3, 4, 5])->get('user')->result();
        $userIds = [];
        foreach ($users as $user) {
            $userIds[] = $user->id;
        }

        if (($countOfUsers = count($userIds)) > 0) {
            echo '<p>Number of users: ' . count($userIds) . '</p>';

            $faker = \Faker\Factory::create();

            $files = glob('files/test/listing/*.{jpg,jpeg,gif,png,JPG,JPEG,GIF,PNG}', GLOB_BRACE);

            $facilities = array_map(function($r){return $r['value'];}, $this->Listings->listFacilities()['data']);
            $amenities = array_map(function($r){return $r['value'];}, $this->Listings->listAmenities()['data']);

            for ($i = 1; $i <= $number; $i++) {
                $userId = $userIds[array_rand($userIds)];
                shuffle($files);
                shuffle($facilities);

                $photos = [];
                for ($j = 0; $j < 3; $j++) {
                    $photos[] = [
                        'title' => 'Title ' . ($j+1),
                        'url' => $files[$j]
                    ];
                }

                $data = [
                    'name' => $faker->sentence(),
                    'address' => $faker->address,
                    'district_id' => mt_rand(1,10),
                    'postal_code' => str_pad(mt_rand(1, 80), 2, '0', STR_PAD_LEFT),
                    'type' => mt_rand(1,3),
                    'property_type' => mt_rand(1,4),
                    'area' => mt_rand(100,1000),
                    'unit_type' => mt_rand(1,2),
                    'tenure' => mt_rand(1,4),
                    'age' => mt_rand(1, 50) * 10,
                    'bath_room' => mt_rand(2, 5),
                    'bed_room' => mt_rand(2, 5),
                    'transportation' => 'Bus',
                    'amenity' => 5,
                    'appointment' => $faker->sentence(),
                    'map_lat' => $faker->latitude(1.29, 1.295),
                    'map_lon' => $faker->longitude(103.8, 103.85),
                    'description' => $faker->paragraphs(5, true),
                    'price' => mt_rand(50, 100) * 5000,
                    'asking_psf' => mt_rand(50, 100) * 5000,
                    'historical' => mt_rand(50, 100) * 5000,
                    'floor' => mt_rand(1,3),
                    'sun' => mt_rand(1,2),
                    'facilities' => array_slice($facilities, 0, mt_rand(0, count($facilities) - 1)),
                    'amenities' => array_slice($amenities, 0, mt_rand(0, count($amenities) - 1)),
                    'note' => $faker->sentence(),
                    'photo' => $files[0],
                    'photos' => $photos,
                    'status' => mt_rand(0,1)
                ];

                if ($data['type'] === 3) {
                    $data['new_launch_data'] = [
                                          "completion_date" => [
                                            "month" => mt_rand(1, 12),
                                            "year" => mt_rand(2016, 2020)
                                          ],
                                          "developer" => "ABC",
                                          "total_number_of_units" => mt_rand(10, 100),
                                          "floor_size_min" => [
                                            "value" => mt_rand(5, 10) * 10,
                                            "unit" => "Sqft"
                                          ],
                                          "floor_size_max" => [
                                            "value" => mt_rand(2, 10) * 100,
                                            "unit" => "Sqft"
                                          ],
                                          "land_size" => [
                                            "value" => mt_rand(10, 300) * 10,
                                            "unit" => "Sqft"
                                          ],
                                          "architect" => "XYZ",
                                          "available_units" => mt_rand(20, 50),
                                          "available_unit_details" => [
                                            [
                                              "description" => "abc",
                                              "area_range" => "100-200",
                                              "unit" => "Sqm"
                                            ]
                                          ]
                                        ];
                }

                $id = $this->Listings->create($data, ['user_id' => $userId]);

                @mkdir($imgPath . '/' . $id, 0777, true);
                @mkdir($imgPath . '/' . $id . '/thumbnail', 0777, true);
                @mkdir($imgPath . '/' . $id . '/medium', 0777, true);

                foreach ($photos as $photo) {
                    $image = new Eventviva\ImageResize($photo['url']);

                    copy($photo['url'], $imgPath . '/' . $id . '/' . basename($photo['url']));
                    
                    $image->resizeToWidth(100);
                    $image->save($imgPath . '/' . $id . '/thumbnail/' . basename($photo['url']));

                    $image->resizeToWidth(400);
                    $image->save($imgPath . '/' . $id . '/medium/' . basename($photo['url']));
                }
            }
        }
    }
}
