<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/API_Controller.php';


class Authentications extends API_Controller
{


    public function facebook_get()
    {
        return $this->getLink('facebook');
    }


    public function google_get()
    {
        return $this->getLink('google');
    }


    private function getLink($provider = NULL)
    {
        $callbackUrl = strval($this->input->get('return_url'));
        if ($callbackUrl !== '') {
            $callbackUrl = '&return_url=' . urlencode($callbackUrl);
        }
        $loginUrl = site_url('auth/index/?provider=' . $provider) . $callbackUrl;

        $this->set_response([
            'result' => TRUE,
            'data' => $loginUrl
        ]);
    }


    public function providers_get($provider = NULL)
    {
        $config    = include(APPPATH . '/config/hybrid_auth.php');
        $providers = [];
        if (!is_null($provider)) {
            $provider = ucfirst($provider);
            if (isset($config['providers'][$provider])) {
                $config['providers'] = [$config['providers'][$provider]];
            }
        }

        foreach ($config['providers'] as $key => $values) {
            if (isset($values['keys'])) {
                if (!empty($values['keys']['id'])) {
                    $id = $values['keys']['id'];
                }
                elseif (!empty($values['keys']['key'])) {
                    $id = $values['keys']['key'];
                }
                else {
                    $id = '';
                }
                if ($id !== '') {
                    $providers[$key] = [
                        'id' => $id
                    ];
                }
            }
        }

        if (is_null($provider)) {
            $data = $providers;
        }
        else {
            $data = empty($providers) ? NULL : $providers[0];
        }

        if (is_null($data)) {
            $response = [
                'result' => FALSE,
                'code' => 404,
                'reason' => 'not_found',
                'message' => 'Not found'
            ];
        }
        else {
            $response = [
                'result' => TRUE,
                'code' => 200,
                'data' => $data
            ];
        }

        $this->set_response($response);
    }
}
