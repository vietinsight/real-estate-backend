<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/API_Controller.php';


class Listings extends API_Controller {

    protected $model = 'Listings';
    protected $selector;


    function __construct()
    {
        parent::__construct();

        $this->selector = $this->Model->selector;
    }


    public function index_get($id = null)
    {
        if (is_null($id)) {
            return parent::index_get($id);
        } else {
            $this->selector .= ', near_by';
            $field = is_numeric($id) ? 'id' : 'uuid';
            $listing = $this->Model->getDetailByField($field, $id, $this->selector);

            if (!is_null($listing)) {
                $listing->near_by = $this->Model->getNearBy($listing->map_lat, $listing->map_lon);

                if ($this->input->get('action') === 'view') {
                    $this->db->set('views', 'views + 1', FALSE)->where('id', $listing->__id)->update($this->Model->table);
                }

                $response = $this->responseDetail($listing, true);
            } else {
                $response = $this->responseDetail(null, true);
            }

            $this->set_response($response);
        }
    }


    public function index_post()
    {
        $this->auth->checkToken();

        $data = $this->input->post_stream();
        $data = $this->getExtendsData($data);
        $res = $this->Model->create($data);

        if (is_int($res) && $res > 0) {
            $response = [
                'result' => true,
                'code' => 201,
                'data' => $this->Model->getDetail($res, $this->selector)
            ];
        } else {
            $response = [
                'result' => false,
                'status' => 400,
                'error' => $res,
                'message' => 'Bad Request: validation errors, missing data',
                'data' => $data
            ];
        }


        $this->set_response($response);
    }


    public function index_put($uuid = null)
    {
        $this->auth->checkToken();

        $data = $this->input->post_stream();
        $data = $this->getExtendsData($data);
        $result = $this->Model->edit($data, ['uuid' => $uuid]);

        $response = [
            'result' => $result
        ];

        if ($result) {
            $result = $this->Model->getDetailByField('uuid', $uuid, $this->selector);
            if ($result) {
                $response['data'] = $result;
            } else {
                $response['result'] = false;
            }
        }

        $this->set_response($response);
    }


    public function drafts_post()
    {
        $this->auth->checkToken();

        $this->load->model('Drafts');
        $data = $this->input->post_stream();
        $id = $this->Drafts->create($data, $this->Model->table);

        $response = [
            'result' => $id > 0,
            'data' => [
                'id' => $id
            ]
        ];
        $this->set_response($response);
    }


    public function drafts_put($id = null)
    {
        $this->auth->checkToken();

        $this->load->model('Drafts');
        $data = $this->input->post_stream();
        $id = $this->Drafts->edit($data, $this->Model->table, $id);

        $response = [
            'result' => $id > 0
        ];
        $this->set_response($response);
    }


    public function drafts_delete($id = null)
    {
        $this->auth->checkToken();

        $this->load->model('Drafts');
        $this->Drafts->trash($id);

        $response = [
            'result' => true
        ];
        $this->set_response($response);
    }


    public function drafts_get($id = null)
    {
        $this->auth->checkToken();

        $this->load->model('Drafts');

        if (is_null($id)) {
            $options = $this->getOptions();
            unset($options['filters'], $options['sorts']);
            $options['selector'] = $this->Drafts->selector;
            $response = $this->Drafts->getList(['user_id' => $this->auth->userId()], $options);
        } else {
            $response = $this->responseDetail($this->Drafts->getDetail($id));
        }

        $this->set_response($response);
    }


    private function getExtendsData($data = [])
    {
        if (!empty($data['postal_code'])) {
            $this->load->library('Esearch');
            $this->esearch->selectType('district');

            $query = [
                'query' => [
                    'match' => [
                        'code' => substr(strval($data['postal_code']), 0, 2)
                    ]
                ]
            ];

            try {
                $res = $this->esearch->search($query);
                if ($res['hits']['total'] > 0) {
                    $data['district_id'] = $res['hits']['hits'][0]['_id'];
                    $data['district_name'] = $res['hits']['hits'][0]['_source']['name'];
                }
            } catch (Exception $e) {}
        }

        $data['quick_pro'] = 123456;

        $data['data'] = json_encode($data);

        $data = $this->getNearBy($data);

        return $data;
    }


    private function getNearBy($data)
    {
        if (!empty($data['map_lat']) && !empty($data['map_lon'])) {
            $res = $this->Model->getNearBy($data['map_lat'], $data['map_lon']);
            if (!empty($res)) {
                $data['near_by'] = json_encode($res);
            }
        }

        return $data;
    }


    public function near_by_get($map_lat = '', $map_lon = '')
    {
        $response = $this->Model->getNearBy($map_lat, $map_lon);

        $this->set_response($response);
    }


    /* Get similar listings */
    public function similars_get($listingId = '')
    {
        $options = $this->getOptions();
        $response = $this->Model->getSimilarNearBy($listingId, $options, false);

        $this->set_response($response);
    }


    /* Get near by projects */
    public function nearby_projects_get($listingId = '')
    {
        $options = $this->getOptions();
        $response = $this->Model->getSimilarNearBy($listingId, $options, true);

        $this->set_response($response);
    }


    /* LIST FACILITIES */
    public function facilities_get()
    {
        $response = $this->Model->listFacilities(NULL, isset($_GET['raw']));
        $this->set_response($response);
    }


    /* LIST AMENITIES */
    public function amenities_get()
    {
        $response = $this->Model->listAmenities(NULL, isset($_GET['raw']));
        $this->set_response($response);
    }


    /* SHORTLIST */
    public function shortlists_post($parentId)
    {
        $this->auth->checkToken();

        $folderId = (int) $this->input->post_stream('folder_id');
        $extra = $folderId > 0 ? ['shortlist_folder' => $folderId] : [];
    	$this->likePOST($parentId, 1, $extra);
    }

    public function shortlists_delete($parentId)
    {
        $this->auth->checkToken();

    	$this->likeDELETE($parentId, 1);
    }


    /* SUBSCRIPTION */
    public function subscriptions_post($parentId)
    {
    	$this->likePOST($parentId, 2);
    }

    public function subscriptions_delete($parentId)
    {
    	$this->likeDELETE($parentId, 2);
    }


    /* RATING */
    public function ratings_post($parentId)
    {
        $this->auth->checkToken();

        $this->load->model('Likes');
        $extra = [];
        $point = (int) $this->input->post_stream('point');
        if ($point >= 1 && $point <= 5) {
            $extra = [$this->Likes->types[3] => $point];
        }

        $this->likePOST($parentId, 3, $extra);
    }

    public function ratings_delete($parentId)
    {
        $this->likeDELETE($parentId, 3);
    }


    public function comments_get($parentId)
    {
    	$res = $this->Model->getDetailCache('uuid', $parentId);
    	$response = ['result' => false];

    	if (!is_null($res)) {
	    	$this->load->model('Comments');
            $userField = [
                'uuid' => 'id',
                'email',
                'name',
                'phone'
            ];
            $userSelector = [];
            foreach ($userField as $key => $value) {
                if (is_int($key)) {
                    $userSelector[] = 'user.' . $value . ' as user_' . $value;
                } else {
                    $userSelector[] = 'user.' . $key . ' as user_' . $value;
                }
            }

            $options = $this->getOptions();
            $options['selector'] = 'comment.uuid as id, user_id, comment.message, comment.time_created, (select count(*) from `comment` as c where `c`.`parent_id` = comment.id) as count_reply';

	    	$response = $this->Comments->getList(
                    [
                        'listing_id' => $res->id,
                        'parent_id' => null
                    ], $options
                );

            // $userField = array_values($userField);
            // foreach ($result['records'] as $idx => $row) {
            //     $user = new stdClass();
            //     foreach ($userField as $field) {
            //         $user->{$field} = $row->{'user_' . $field};
            //         unset($row->{'user_' . $field});
            //     }
            //     $row->user = $user;
            //     $result['records'][$idx] = $row;
            // }
    	}

    	$this->set_response($response);
    }

    public function comments_post($parentId)
    {
        $this->auth->checkToken();

    	$res = $this->Model->getDetailCache('uuid', $parentId);
    	if (is_null($res)) {
    		$response = ['result' => false];
    	} else {
	    	$this->load->model('Comments');
	    	$data = $this->input->post_stream();
	    	$userId = $this->auth->userId();
	    	if ($userId) {
	    		$data['user_id'] = $userId;
	    	}
	    	$data['listing_id'] = $res->id;
	    	$id = $this->Comments->create($data);

	    	if ($id) {
	    		$response = [
	    				'result' => true,
	    				'data' => $this->Comments->getDetail($id, 'uuid as id, message, user_id, listing_id, time_created')
	    			];
	    	}
    	}

    	$this->set_response($response);
    }

    public function comments_delete($parentId, $commentId = null)
    {
        $this->auth->checkToken();
        
    	$res = $this->Model->getDetailCache('uuid', $parentId);
    	$response = ['result' => true];

    	if (!is_null($res)) {
	    	$this->load->model('Comments');
	    	$this->Comments->delete([
	    			'uuid' => $commentId,
	    			'user_id' => $this->auth->userId()
	    		]);
    	}

    	$this->set_response($response);
    }


    private function likePOST($parentId, $type, $extra = [])
    {
        $this->auth->checkToken();

    	$result = false;
        $field = is_numeric($parentId) ? 'id' : 'uuid';
    	$res = $this->Model->getDetailCache($field, $parentId);

    	if ($res) {
    		$this->load->model('Likes');
            $Type = $this->Likes->types[$type];
            $data = [];

    		$wheres = [
    				'user_id' => $this->auth->userId(),
    				'listing_id' => $res->id
    			];
            $row = $this->Likes->get($wheres);

            $data[$Type] = 1;
            $data['time_' . $Type] = date('Y-m-d H:i:s');
            $data = array_merge($data, $extra);

    		if ($row) {
                if ($row->{$Type} === '0' OR $type === 3 OR !empty($extra)) {
                    $this->Likes->update($data, $row->id);
                    $this->Model->calculateRating($res->id);
                }
    		} else {
                $data = array_merge($data, $wheres);
                $this->Likes->create($data);
                $this->Model->calculateRating($res->id);
            }
    	}

    	$this->set_response(['result' => true]);
    }


    private function likeDELETE($parentId, $type)
    {
        $this->auth->checkToken();

    	$result = false;
    	$res = $this->Model->getDetailCache('uuid', $parentId);

    	if ($res) {
    		$this->load->model('Likes');
            $Type = $this->Likes->types[$type];
            $data = [
                    $Type => 0,
                    'time_' . $Type => null
                ];
            $where = [
                    'user_id' => $this->auth->userId(),
                    'listing_id' => $res->id
                ];

			$this->Likes->update($data, $where);
    	}

    	$this->set_response(['result' => true]);
    }


    public function properties_post($listingId = null)
    {
        $this->auth->checkToken();

        $response = [
                'result' => true
            ];
        $this->set_response($response);
    }


    public function wishlists_post()
    {
        $this->auth->checkToken();
        
        $response = [
                'result' => true
            ];
        $this->set_response($response);
    }


    /* TODO */
    public function historical_pricing_get($listingId = null)
    {
        if (is_null($listingId)) {
            $response = [
                'result' => false,
                'code' => 400,
                'reason' => 'not_found',
                'message' => 'listing id is invalid'
            ];
        } else {
            $field = is_numeric($listingId) ? 'id' : 'uuid';
            $listing = $this->Model->getDetailCache($field, $listingId);
            $response = [
                'result' => true
            ];
        }

        $this->set_response($response);
    }


    public function past_transactions_get($listingId = null)
    {
        $notFound = true;
        $response = [];

        if (!is_null($listingId)) {
            $field = is_numeric($listingId) ? 'id' : 'uuid';
            $listing = $this->Model->getDetailCache($field, $listingId);
            if ($listing) {
                $notFound = false;
//                $response = $this->db->get
            }
        }

        if ($notFound) {
            $response = $this->responseDetail(null, true);
        }

        $this->set_response($response);
    }


    public function export_get($listingId = null, $type = 'excel')
    {
        $filePath = 'files/export/listing_' . $listingId . '.xls';

        if (!is_null($listingId) && file_exists($filePath) === false) {
            $this->load->model('Listings');
            $listing = $this->Listings->getDetailCache('uuid', $listingId);
            if (!is_null($listing)) {
                require_once APPPATH . '/third_party/PHPExcel/PHPExcel.php';

                // Create new PHPExcel object
                $objPHPExcel = new PHPExcel();

                // Set document properties
                $objPHPExcel->getProperties()->setCreator('Smart Property')
                                             ->setLastModifiedBy('Smart Property')
                                             ->setTitle('Export Listing To Excel')
                                             ->setSubject('Export Listing To Excel')
                                             ->setDescription('Export Listing To Excel')
                                             ->setKeywords('Export Listing To Excel')
                                             ->setCategory('Export Listing To Excel');

                $dataArr = [
                    'quick_pro' => 'QuikPro No',
                    'name' => 'Name',
                    'district_name' => 'District',
                    'property_type' => 'Property Type',
                    'price_psf' => 'Asking Price',
                    'area' => 'Built up',
                    'unit_type' => 'Unit Type',
                    'tenure' => 'Tenure',
                    'age' => 'Age',
                    'bath_room' => 'Bedrooms',
                    'bed_room' => 'Bathrooms',
                    'map_lat' => 'Latitude',
                    'map_lon' => 'Longitude'
                ];

                // Add some data
                $excel = $objPHPExcel->setActiveSheetIndex(0);

                $excel->setCellValue('A1', 'Smart Property');

                $row = 3;
                foreach ($dataArr as $key => $label) {
                    $excel->setCellValue('A' . $row, $label)
                            ->setCellValue('B' . $row, $listing->{$key});
                    $row++;
                }

                foreach(['A', 'B'] as $columnID) {
                    $excel->getColumnDimension($columnID)->setAutoSize(true);
                }


                // Rename worksheet
                // $objPHPExcel->getActiveSheet()->setTitle('Simple');


                // Set active sheet index to the first sheet, so Excel opens this as the first sheet
                $objPHPExcel->setActiveSheetIndex(0);


                // // Redirect output to a client’s web browser (Excel5)
                // header('Content-Type: application/vnd.ms-excel');
                // header('Content-Disposition: attachment;filename="listing_' . $listingId . '.xls"');
                // header('Cache-Control: max-age=0');
                // // If you're serving to IE 9, then the following may be needed
                // header('Cache-Control: max-age=1');

                // // If you're serving to IE over SSL, then the following may be needed
                // header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
                // header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
                // header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
                // header ('Pragma: public'); // HTTP/1.0

                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                // $objWriter->save('php://output');
                $objWriter->save($filePath);
            }
        }
        
        $this->set_response([
                'result' => true,
                'data' => site_url($filePath)
            ]);
    }


    public function exclusives_get()
    {
        $wheres = [
            'price_psf >' => 3000
        ];
        $options = $this->getOptions();
        $response = $this->Model->getList($wheres, $options);

        $this->set_response($response);
    }


    public function belows_get()
    {
        $options = $this->getOptions();
        $options['joins'][] = ['listing_psf', 'listing_psf.loc_key = ' . $this->Model->table . '.postal_code'];
        $wheres = [
            ['price_psf <= listing_psf.psf * 0.8', null, false]
        ];
        $response = $this->Model->getList($wheres, $options);

        $this->set_response($response);
    }


    public function new_listing_get()
    {
        $wheres = [
            'status' => 1,
            'type !=' => 3,
            'time_created >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)' => null
        ];
        $options = $this->getOptions();
        $response = $this->Model->getList($wheres, $options);

        $this->set_response($response);
    }


    public function new_launch_get()
    {
        $wheres = [
            'status' => 1,
            'type' => 3
        ];
        $options = $this->getOptions();
        $response = $this->Model->getList($wheres, $options);

        $this->set_response($response);
    }


    public function new_launch_agent_get($listingId = null)
    {
        $listing = $this->Model->getDetailCache('uuid', $listingId);
        if (is_null($listing)) {
            $response = [
                'result' => false,
                'code' => 404,
                'reason' => 'not_found'
            ];
        } else {
            $wheres = [
                'id !=' => $listing->id,
                'type' => 3,
                'postal_code' => $listing->postal_code,
            ];
            $options = $this->getOptions();
            $options['selector'] = 'id as __id, uuid as id, name, address, photo, photos, description, user_id';
            $options['group_by'][] = 'user_id';
            $res = $this->Model->getList($wheres, $options);

            $agents = array_map(function($r){
                $agent = $r->user;
                unset($r->user, $r->photos);
                $agent->listings = [$r];
                return $agent;
            }, $res['records']);

            if (isset($agents['paging']['total']) && $agents['paging']['total'] > 0) {
                $agents['paging']['total']--;
            }

            if (empty($agents)) {
                $response = [
                    'result' => false,
                    'code' => 204
                ];
            } else {
                $response = [
                    'result' => true,
                    'code' => 200,
                    'data' => $agents
                ];
            }
        }
        
        $this->set_response($response);
    }


    public function premium_get()
    {
        $options = $this->getOptions();
        $wheres = [
            'price_psf >=' => 3000
        ];
        $response = $this->Model->getList($wheres, $options);

        $this->set_response($response);
    }


    public function wishlists_get()
    {
        $this->auth->checkToken();

        $this->load->model('Listings');
        $this->load->model('Users');
        $userId = $this->auth->userId();
        $wishlist = $this->Users->getWishlist($userId);
        if (is_null($wishlist)) {
            $response = [
                'result' => false,
                'code' => 204,
                'reason' => 'no_data',
                'message' => 'No data'
            ];
        } else {
            $wheres = [];
            foreach ($wishlist as $key => $value) {
                if (is_array($value)) {
                    if (count($value) >= 2 && is_numeric($value[0]) && is_numeric($value[1])) {
                        $wheres[$key . ' >='] = $value[0];
                        $wheres[$key . ' <='] = $value[1];
                    }
                } elseif (is_scalar($value)) {
                    $wheres[$key] = $value;
                }
            }

            $options = $this->getOptions();
            $options['selector'] = $this->Listings->selector;
            $response = $this->Listings->getList($wheres, $options);
        }

        $this->set_response($response);
    }


    public function appointments_post($listingId = NULL)
    {
        $this->auth->checkToken();

        $data = $this->input->post_stream();
        $error = null;

        $field = is_numeric($listingId) ? 'id' : 'uuid';
        $listing = $this->Model->getDetailRawCache($field, $listingId, 'id, user_id');
        if ($listing) {
            $data['user_id'] = $this->auth->userId();
            $data['partner_id'] = $listing->user_id;
            $data['listing_id'] = $listing->id;
            $this->load->model('Appointments');
            $res = $this->Appointments->create($data);

            if (is_numeric($res)) {
                $response = [
                    'result' => true,
                    'code' => 201,
                    'data' => [
                        'id' => $res
                    ]
                ];
            } else {
                $response = [
                    'result' => false,
                    'status' => 400,
                    'reason' => 'bad_request',
                    'errors' => $res,
                    'message' => 'Bad Request: validation errors, missing or exist data'
                ];
            }
        } else {
            $response = parent::responseDetail(null, true);
        }

        $this->set_response($response);
    }


    public function appointments_get($listingId = NULL, $own = null)
    {
        $this->auth->checkToken();

        $field = is_numeric($listingId) ? 'id' : 'uuid';
        $listing = $this->Model->getDetailCache($field, $listingId);
        if ($listing) {
            $this->load->model('Appointments');
            $options = $this->getOptions();
            $options['selector'] = $this->Appointments->selector;
            $wheres = [
                'listing_id' => $listing->id
            ];

            $userId = $this->auth->userId();
            if (is_null($own)) {
                $wheres['partner_id'] = $userId;
            } elseif ($own === 'me') {
                $wheres['user_id'] = $userId;
            } elseif ($own === 'all') {
                $wheres['(user_id = "' . $userId . '" OR partner_id = "' . $userId . '")'] = null;
            } else {
                $wheres = null;
            }

            if (is_null($wheres)) {
                $response = parent::responseDetail(null, true);
            } else {
                if (!empty($_GET['date']) && preg_match('#^\d{4}-\d{1,2}-\d{1,2}$#', $_GET['date'])) {
                    $wheres['DATE(`booking_time`)'] = $_GET['date'];
                }
                $response = $this->Appointments->getList($wheres, $options);
            }
        } else {
            $response = parent::responseDetail(null, true);
        }

        $this->set_response($response);
    }


    public function timelines_post($listingId = null, $type = 'free')
    {
        $this->auth->checkToken();

        $field = is_numeric($listingId) ? 'id' : 'uuid';
        $listing = $this->Model->getDetailRawCache($field, $listingId);
        if ($listing) {
            $this->load->model('Appointments');
            $data = $this->input->post_stream();
            $res  = $this->Appointments->setFreeTime($data, $listingId);

            if ($res) {
                $response = [
                    'result' => true,
                    'code' => 201
                ];
            } else {
                $response = [
                    'result' => false,
                    'code' => 400,
                    'reason' => 'bad_request'
                ];
            }
        } else {
            $response = $this->responseDetail(null, true);
        }

        $this->set_response($response);
    }


    public function timelines_get($listingId = null)
    {
        if (is_null($listingId)) {
            $response = [
                'result' => false,
                'code' => 204,
                'reason' => 'empty',
                'message' => 'No data timeline for this listing (' . $listingId .')'
            ];
        } else {
            $this->load->model('Appointments');
            $field = is_numeric($listingId) ? 'id' : 'uuid';
            $listing = $this->Model->getDetailRawCache($field, $listingId);
            if ($listing) {
                $res = $this->Appointments->getTimeLine($listing->id);
                if ($res) {
                    $this->load->model('Users');
                    $user = $this->Users->getDetailCache('id', $listing->user_id, 'id as __id, uuid as id, name, email, phone, type, avatar');
                    $response = [
                        'result' => true,
                        'code' => 200,
                        'data' => [
                            'user' => $user,
                            'listing' => [
                                '__id' => $listing->id
                            ],
                            'free_times' => $res
                        ]
                    ];
                } else {
                    $response = [
                        'result' => false,
                        'code' => 204,
                        'reason' => 'empty',
                        'message' => 'No data timeline for this listing (' . $listingId .')'
                    ];
                }
            } else {
                $response = $this->responseDetail(null, true);
            }
        }

        $this->set_response($response);
    }
}
