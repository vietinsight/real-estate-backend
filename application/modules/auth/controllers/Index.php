<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Index extends CI_Controller
{

    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('Auth');
    }

    public function index()
    {
        $response       = [];
        $provider       = NULL;
        $providerParams = [
            'provider',
            'hauth_start',
            'hauth_done'
        ];
        foreach ($providerParams as $fParam) {
            if (!empty($_GET[$fParam])) {
                $provider = $_GET[$fParam];
                break;
            }
        }

        if (!is_null($provider)) {
            $this->load->library('HybridAuth');
            if (isset($_REQUEST['hauth_start']) || isset($_REQUEST['hauth_done'])) {
                require_once(APPPATH . '/third_party/Hybrid/Endpoint.php');
                Hybrid_Endpoint::process();
            }
            else {
                try {
                    $userProfile = $this->hybridauth->authenticate($provider);
                    $email       = empty($userProfile->email) ? NULL : $userProfile->email;

                    if (!is_null($email)) {
                        $this->load->model('Users');
                        $user               = $this->Users->getDetailCache('email', $email);
                        $response['status'] = is_null($user) ? 'New' : 'Exist';
                        if (is_null($user)) {
                            $provider = strtolower($provider);
                            $userData = [
                                'email' => $email,
                                'password' => md5(base64_encode(mcrypt_create_iv(32))),
                                'social' => $provider,
                                'name' => empty($userProfile->displayName) ? $provider . '_' . $userProfile->identifier : $userProfile->displayName,
                                'type' => 'Buyer',
                                'status' => 1
                            ];
                            $userId   = $this->Users->create($userData);
                            $user     = $this->Users->getDetailCache('id', $userId);
                        }
                        $response['token'] = $this->auth->generateToken($user, 20);
                    }
                    else {
                        $response = ['error' => 'no_email'];
                    }
                }
                catch (Exception $e) {
//					echo "Ooophs, we got an error: " . $e->getMessage();
                    $response = [
                        'error' => 'no_data',
                        'message' => $e->getMessage()
                    ];
                }
            }
        }
        else {
            $response = ['error' => 'provider_invalid'];
        }

        redirect(getReturnLink($response));
    }
}
