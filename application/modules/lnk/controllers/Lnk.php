<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Lnk extends CI_Controller
{


    public function users($action = '', $email, $iat = 0, $token = '')
    {
        $email    = urldecode($email);
        $iat      = (int) $iat;
        $chkToken = genToken('activate', $email, $iat);
        if ($chkToken === $token) {
            if ($iat + 259200 >= time()) {
                // valid
            }

            $this->load->model('Users');
            $user = $this->Users->getDetailByField('email', $email);
            if (!is_null($user)) {
                $this->Users->update(['status' => 1], ['id' => $user->id]);

                $this->load->library('Auth');
                $params = [
                    'token' => $this->auth->generateToken($user, 10),
                    'status' => $user->status,
                    'name' => $user->name
                ];

                redirect(getReturnLink($params));
            }
            else {
                redirect(getReturnLink([
                    'error' => 'account_not_exist'
                ]));
            }
        }
        else {
            redirect(getReturnLink([
                'error' => 'token_invalid'
            ]));
        }
    }


    public function unsubscribe($email = NULL, $time = NULL, $token = NULL)
    {
        $this->load->model('Subscribes');

        $res = $this->Subscribes->handle($email, $time, $token, FALSE);

        $config = $this->config->load('common', TRUE);

        $params = [
            'success' => $res ? 'true' : 'false',
            'status' => 'unsubscribe'
        ];

        redirect($config['web_url'] . $config['web_uri_subscribe'] . '/?' . http_build_query($params));
    }


    public function forget_password($email = null, $time = null, $hash = null, $token = null)
    {
        $email = urldecode($email);
        $time  = (int) $time;
        $hash  = (string) $hash;
        $token = (string) $token;

        if (genToken('forgetPassword', $email, $time, $hash) === $token) {
            if (time() - $time <= 259200) {
                $this->load->model('Users');
                $user = $this->Users->getDetailRawCache('email', $email);
                if ($user) {
                    $userHash = (string) $user->access_token;
                    if ($userHash === '') {
                        $userHash = '0';
                    }
                    
                    if ($hash == $userHash) {
                        $this->load->model('Emails');
                        $this->Emails->resetPassword($user);

                        echo 'Please check your email to get new password';
                    } else {
                        echo 'Already';
                    }
                }
            } else {

            }
        } else {

        }
    }


    public function tables($table = NULL)
    {
        if ((!empty($_GET['auth']) && $_GET['auth'] === 'justForDev') === FALSE) {
            die();
        }

        $this->load->database();

        $tables = [
            'appointment',
            'comment',
            'listing',
            'loan',
            'user'
        ];

        $showTables = [];

        if (is_null($table)) {
            $showTables = $tables;
        }
        else if (in_array($table, $tables)) {
            $showTables = [$table];
        }
        else {
            die('Table ' . $table . ' is not exist or hide for security!');
        }

        $protectedFields = [
            'id',
            'uuid',
            'user_id',
            'idxed',
            'data'
        ];

        foreach ($showTables as $tableS) {
            $data = $this->db->query('DESCRIBE ' . $tableS)->result();
            $this->load->model(ucwords($tableS) . 's', $tableS);
            $mapping = $this->{$tableS}->getMappingFields();

            $data = array_map(function ($r) use ($tableS, $mapping, $protectedFields) {
                if (!in_array($r->Field, $protectedFields)) {
                    $mapValue = isset($mapping[$r->Field]) ? $mapping[$r->Field] : [];
                    if (isset($mapping[$r->Field])) {
                        $obj              = new stdClass();
                        $obj->{$r->Field} = $r->Default;
                        $convert          = $this->{$tableS}->convertOutput($obj);
                        if (!empty($convert->{$r->Field})) {
                            $r->Default = $convert->{$r->Field};
                        }
                    }
                    $r->Value = is_array($mapValue) ? implode(', ', $mapValue) : $mapValue;

                    return $r;
                }
            }, $data);
            $data = array_filter($data, function ($r) {
                if (!is_null($r)) {
                    return $r;
                }
            });
            $data = array_values($data);


            $this->load->view('tools/tables', [
                'table' => $tableS,
                'rows' => $data
            ]);
        }
    }
}
