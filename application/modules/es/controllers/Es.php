<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Es extends CI_Controller
{
    public function index()
    {
        header('Content-Type: application/json');

        $resource = implode('/', func_get_args());
        $query = $this->input->server('QUERY_STRING');

        $res = eSearch($resource . '?' . $query);

        echo json_encode($res);
    }
}
