<?php

defined('BASEPATH') OR exit('No direct script access allowed');


function getReturnLink($params = [])
{
	$ci =& get_instance();

	$gets = $ci->input->get();

	if (empty($gets['return_url'])) {
		$config = $ci->config->load('common', true);
		$url = $config['web_url'] . $config['web_uri_activation'];
	} else {
		$url = $gets['return_url'];
		unset($gets['return_url']);
	}

	$gets = array_merge($gets, $params);

	return $url . (empty($gets) ? '' : (strpos($url, '?') ? '&' : '?') . http_build_query($gets));
}


function eSearch($path = '', $data = [], $method = '')
{
	$ci =& get_instance();
	$config = $ci->config->load('elastic_search', true);
	$url = $config['host'] . ':' . $config['port'] . '/' . (empty($config['index']) ? '' : $config['index']) . '/' . $path;

	return curl($url, $data, $method);
}