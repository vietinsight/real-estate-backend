<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config = [
	'web_url' => 'http://smartproperty_fe.dev',
	'web_uri_activation' => '/users/activation',
	'web_uri_subscribe' => '/users/subscribe',
	'web_uri_term_privacy' => '/termprivacy'
];