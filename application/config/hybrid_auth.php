<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * HybridAuth
 * http://hybridauth.sourceforge.net | http://github.com/hybridauth/hybridauth
 * (c) 2009-2015, HybridAuth authors | http://hybridauth.sourceforge.net/licenses.html
 */
// ----------------------------------------------------------------------------------------
//	HybridAuth Config file: http://hybridauth.sourceforge.net/userguide/Configuration.html
// ----------------------------------------------------------------------------------------

return array(
    "base_url" => "",
    "providers" => array(
        // openid providers
        "OpenID" => array(
            "enabled" => TRUE
        ),
        "Yahoo" => array(
            "enabled" => TRUE,
            "keys" => array(
                "key" => "",
                "secret" => ""
            ),
        ),
        "AOL" => array(
            "enabled" => TRUE
        ),
        "Google" => array(
            "enabled" => TRUE,
            "keys" => array(
                "id" => '222788566501-2ikk9sum0jvi24o3jjhautqfai8assc3.apps.googleusercontent.com',
                "secret" => 'B8CrZqi3B9n3iIodtSmQyhiK'
            ),
            'domain' => 'global'
        ),
        "Facebook" => array(
            "enabled" => TRUE,
            "keys" => array(
                "id" => '328489600860979',
                "secret" => '88fc3191341575e9e7246a83f5fab744'
            ),
            "trustForwarded" => TRUE,
            'scope' => 'email, user_birthday'
        ),
        "Twitter" => array(
            "enabled" => TRUE,
            "keys" => array(
                "key" => "",
                "secret" => ""
            ),
            "includeEmail" => FALSE
        ),
        // windows live
        "Live" => array(
            "enabled" => TRUE,
            "keys" => array(
                "id" => "",
                "secret" => ""
            )
        ),
        "LinkedIn" => array(
            "enabled" => TRUE,
            "keys" => array(
                "key" => "",
                "secret" => ""
            )
        ),
        "Foursquare" => array(
            "enabled" => TRUE,
            "keys" => array(
                "id" => "",
                "secret" => ""
            )
        ),
    ),
    // If you want to enable logging, set 'debug_mode' to true.
    // You can also set it to
    // - "error" To log only error messages. Useful in production
    // - "info" To log info and error messages (ignore debug messages)
    "debug_mode" => FALSE,
    // Path to file writable by the web server. Required if 'debug_mode' is not false
    "debug_file" => "log.txt",
);
