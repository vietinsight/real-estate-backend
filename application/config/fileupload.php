<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['host'] = '127.0.0.1';
$config['port'] = '';
$config['user'] = '';
$config['pass'] = '';
$config['uploadDir'] = 'files/upload';
$config['storageDir'] = 'files';
$config['coverFileName'] = 'cover_file.png';