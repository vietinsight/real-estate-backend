#!/usr/bin/python

import sys
import time
from subprocess import check_output
import json
import csv
import MySQLdb
import os, fnmatch

database = check_output(['php', '-r', 'define("BASEPATH", 1); define("ENVIRONMENT", ""); include "application/config/database.php"; echo json_encode($db);'])
database = json.loads(database)
dbconfig = database['default']

db = MySQLdb.connect(dbconfig['hostname'], dbconfig['username'], dbconfig['password'], dbconfig['database'])
cursor = db.cursor()

if len(sys.argv) > 1:
	ptype = sys.argv[1]
else:
	ptype = True


path = 'files/history/'


print '\n=================================================='
print 'IMPORT CSV TO MYSQL'
print '==================================================\n'

startTime = time.time()

if ptype == 'hdb' or ptype == True:
    print 'HDB Property'
    table = 'history_hdb'
    with open(path + 'resale.csv', 'rb') as csvfile:
        data = csv.reader(csvfile, delimiter=',')
        i = 0
        cursor.execute('TRUNCATE table `' + table + '`');

        for row in data:
            if i > 0:
                try:
                    row[0] +='-01'
                    cursor.execute("INSERT `" + table + "` VALUES(null, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", row)
                    print '------------------'
                    print row
                    print '------------------'
                except Exception as e:
                    print e
                    db.rollback()

            i += 1

if ptype == 'home' or ptype == True:
    print 'Private Home Property'
    table = 'history_home'
    cursor.execute('TRUNCATE table `' + table + '`');
    files = fnmatch.filter(os.listdir(path), 'ura*.csv')

    fileNum = 0
    for file in files:
        fileNum += 1
        pathFile = path + file
        print 'Opening file', pathFile

        with open(pathFile, 'rb') as csvfile:
            data = csv.reader(csvfile, delimiter=',')
            start = False
            for row in data:

                if len(row) > 0:

                    if row[0].isdigit():
                        start = True
                        del row[0]
                        r = []
                        for z in row:
                            r.append(z)

                        try:
                            cursor.execute("INSERT `history_home` VALUES(null, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", r)
                            print '------------------'
                            print r
                            print '------------------'

                        except Exception as e:
                            print e
                            db.rollback()
                    else:
                        if start == True:
                            print '[No ', fileNum, '] Next file...'
                            break

db.commit()
db.close()


print '\n------------------------------'
print 'Elapsed time: ' + str(time.time() - startTime) + ' second(s)'
print '------------------------------'