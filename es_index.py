#!/usr/bin/python

import sys
from subprocess import check_output
import time
import json
import csv
#import MySQLdb
import requests

if len(sys.argv) > 1:
	ptype = sys.argv[1]
else:
	ptype = True


config = check_output(['php', '-r', 'define("BASEPATH", 1); define("ENVIRONMENT", ""); include "application/config/elastic_search.php"; echo json_encode($config);'])
config = json.loads(config)
path = 'files/import/'

esUrl = config['host'] + ':' + str(config['port']) + '/' + config['index']

def readCSV(pathFile):
    with open(pathFile, 'rb') as file:
        return csv.reader(file, delimiter=',')

def create(uri, data={}):
    """Create new document."""
    query = json.dumps(data)
    response = requests.post(uri, data=query)
    # print(response)


print '\n=================================================='
print 'IMPORT CSV TO ELASTIC SEARCH ' + esUrl
print '** Run with parameter: reset|all|agent|district|project|hdb'
print '==================================================\n'

startTime = time.time()

if ptype == 'reset':
    print '\n------------------------------'
    print 'Cleaning ES index...'
    requests.delete(esUrl)
    print '------------------------------\n'


if ptype == 'agent' or ptype == True:
    with open(path + 'EStateList.csv', 'rb') as csvfile:
        fileData = csv.reader(csvfile, delimiter=',')
        print 'Indexing...'
        i = 1;
        for row in fileData:
            # print row
            data = {
                "name": row[0],
                "license": row[1],
                "valid_from": row[2],
                "valid_to": row[3]
            }
            print 'Line: ' + str(i)
            print data
            create(esUrl + '/agent/' + str(i), data)
            i += 1

        print 'Done'
        print '------------------------------'


if ptype == 'district' or ptype == True:
    with open(path + 'district.csv', 'rb') as csvfile:
        fileData = csv.reader(csvfile, delimiter=',')
        print 'Indexing...'
        i = 1;
        for row in fileData:
            # print row
            code = row[0].split(',')
            codes = []
            for c in code:
                codes.append(c.strip())
            data = {
				"id": i,
                "no": "District " + str(i),
                "name": row[1],
                "code": codes
            }
            print 'Line: ' + str(i)
            print data
            create(esUrl + '/district/' + str(i), data)
            i += 1

        print 'Done. Total: ' + str(i-1)
        print '------------------------------'


if ptype == 'project' or ptype == True:
    with open(path + 'projects.csv', 'rb') as csvfile:
        fileData = csv.reader(csvfile, delimiter=',')
        print 'Indexing...'
        i = 1;
        for row in fileData:
            if len(row) > 0:
                # print row
                data = {
                    "name": row[0].strip()
                }
                print 'Line: ' + str(i)
                print data
                create(esUrl + '/project/' + str(i), data)
                i += 1

        print 'Done. Total: ' + str(i-1)
        print '------------------------------'


if ptype == 'hdb' or ptype == True:
    with open(path + 'hdb.csv', 'rb') as csvfile:
        fileData = csv.reader(csvfile, delimiter=',')
        print 'Indexing...'
        i = 1;
        for row in fileData:
            if len(row) >= 2:
                # print row
                data = {
					"id": i,
                    "block": row[0].strip(),
                    "street_name": row[1].strip()
                }
                print 'Line: ' + str(i)
                print data
                create(esUrl + '/hdb/' + str(i), data)
                i += 1

        print 'Done. Total: ' + str(i-1)
        print '------------------------------'


print '\n------------------------------'
print 'Elapsed time: ' + str(time.time() - startTime) + ' second(s)'
print '------------------------------'